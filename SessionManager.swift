//
//  SessionManager.swift
//  SMS_VERF
//
//  Created by sudheer kumar meesala on 9/7/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//
import Foundation

class SessionManager {
    
    var baseUrl = Constants.baseUrl
    var persistenceStore: PersistenceStore?
    class var sharedsessionManager : SessionManager {
        
        struct Static {
            static var instance : SessionManager?
            static var token: dispatch_once_t = 0
        }
        
        dispatch_once (&Static.token) {
            
            Static.instance = SessionManager()
            Static.instance!.userSession = UserSession()
            Static.instance!.persistenceStore = PersistenceStore()
        }
            
        return Static.instance!
    }
    
    internal var userSession : UserSession?
    
    func setUserInfo(userInfo: UserInfo){
        if(self.userSession != nil){
            self.userSession!.userInfo = userInfo
            self.persistenceStore?.persistUserInfo(userInfo)
        }
        else{
            println("user session is nil, so cannot set user info")
        }
    }
    func setAuthToken(authToken: String){
        if(self.userSession != nil){
            self.userSession!.authToken = authToken
            self.persistenceStore?.persistAuthToken(authToken)
        }
        else{
            println("user session is nil, so cannot set auth token")
        }
    }
    func loadUserInfoFromStore(){
        self.userSession!.userInfo = self.persistenceStore!.getUserInfo()
        self.userSession!.authToken = self.persistenceStore!.getAuthToken()
    }
    func registerUser (userInfo : UserInfo) {
        self.userSession!.userInfo = userInfo
        println(SessionManager.sharedsessionManager.userSession)
        request(.POST, baseUrl+"useradd", parameters:  userInfo.toDict(), encoding: .JSON)
            .responseString {(request, response, string, error) in
                if  error != nil{
                    println(error)
                } else{
                    PersistenceManager.sharedPersistenceManager.persistUserInfo(userInfo)
                }
        }
    }
    
    func logOut(){
        self.userSession = UserSession()
        persistenceStore!.clearAllSavedResources()
    }
    
}