//
//  verificationViewController.swift
//  MeetUp
//
//  Created by Pravin Gadakh on 9/20/14.
//  Copyright (c) 2014 Pravin Gadakh. All rights reserved.
//

import UIKit
import Foundation

class verificationViewController: UIViewController {
    
    var verificationPresenter: VerificationPresenter?
    @IBOutlet weak var verificationField: UITextField!
    @IBOutlet weak var verifyButton: UIButton!
    var phone: String  = ""
    var countryCode: String = ""
    var flag: Int = 0
    var sessionManager = SessionManager.sharedsessionManager
    var persistenceManager = PersistenceManager.sharedPersistenceManager
    var verificationManager  = VerificationManger()
    @IBAction func editingChanged(sender : AnyObject){
        var phoneNumber = self.verificationField.text
        if phoneNumber.isEmpty == true {
            self.verifyButton.enabled = false
        }else{
            self.verifyButton.enabled = true
        }
    }
    
    @IBAction func verifyButton(sender : AnyObject) {
        self.verificationField.resignFirstResponder()
        println("verifying user")
        var verification =  Verification()
        if self.flag == 0 {
            verification.phoneInfo = self.verificationPresenter!.getUserPhoneInfo()
        } else {
            verification.phoneInfo = PhoneInfo(countryCode: self.countryCode, phoneNumber: self.phone)
        }
        verification.verificationString = self.verificationField.text;
        self.verifyButton.enabled = false
        verificationPresenter?.verifyUser(verification,flag: flag, verifyUserCompletionHandler: { [unowned self](isVerified) -> () in
            if(isVerified == true){
                println("verified successfully")
                self.performSegueWithIdentifier("verified", sender: self)
            }
            else{
                println("cant verify")
            }
            NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                self.verifyButton.enabled = true
            })
        })
    }
    
    func textFieldShouldReturn(textField : UITextField! )-> Bool{
        self.verificationField.resignFirstResponder()
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.verificationPresenter = VerificationPresenter()
        self.verificationPresenter!.verificationManager = VerificationManger()
        self.verificationPresenter!.sessionManager = SessionManager.sharedsessionManager
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent){
        self.view.endEditing(true)
    }
    
    /*override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "pushContact" {
            var contactController : ContactsController = segue.destinationViewController as ContactsController
            if self.flag == 0 {
                contactController.phone = SessionManager.sharedsessionManager.userSession!.userInfo!.userPhoneNumber
            } else {
                contactController.phone = self.phone
            }
            contactController.flag = 1
        }
    }*/
}

