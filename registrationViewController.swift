//
//  registrationViewController.swift
//  MeetUp
//
//  Created by Pravin Gadakh on 9/20/14.
//  Copyright (c) 2014 Pravin Gadakh. All rights reserved.
//

import UIKit
import Foundation

class registrationViewController: UIViewController,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource{
    var registrationPresenter: RegistrationPresenter?
    var countryCodesHelper: CountryCodesHelper = CountryCodesHelper()
    var selectedValue:String = ""
    @IBOutlet weak var userNameFT: UITextField!
    @IBOutlet weak var phoneNumberFT: UITextField!
    @IBOutlet weak var emailFT: UITextField!
    
    @IBOutlet weak var registerButton: UIButton!
    
    @IBOutlet weak var countryNameFT: UITextField!
    var countryPickerView: UIPickerView = UIPickerView()
    @IBAction func editingChanged (sender : AnyObject) {
        var phoneNumber = self.phoneNumberFT.text
        var userName = self.userNameFT.text
        var email = self.emailFT.text
        if(self.registrationPresenter?.validateUserDetails(phoneNumber, userName: userName, email: email) == true){
            self.registerButton.enabled = true
        }
        else{
            self.registerButton.enabled = false
        }
    }
    
    @IBAction func registerUser (sender: AnyObject) {
        self.phoneNumberFT.text =  self.phoneNumberFT.text
        var phoneNumber : String = self.phoneNumberFT.text!
        var userName : String = self.userNameFT.text
        var email : String = self.emailFT.text
        var countryCode = countryCodesHelper.countryCodes[self.countryNameFT.text]!.dial_code
        var userInfo = UserInfo (userName: userName,phoneInfo: PhoneInfo(countryCode:countryCode,phoneNumber: phoneNumber),emailId: email)
        registrationPresenter?.registerUser(userInfo, registrationCompleteHandler: { (error) -> () in
            if(error != nil){
                println("could not register user")
            }
        })
    }

    @IBAction func touchDown(sender: AnyObject) {
        self.view.resignFirstResponder()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        countryNameFT.inputView = countryPickerView
        countryCodesHelper.getCountryCodes()
        countryPickerView.dataSource = self
        countryPickerView.delegate = self
        self.registrationPresenter = RegistrationPresenter()
        self.registrationPresenter!.registrationManager = RegistrationManager()
        self.registrationPresenter!.sessionManager = SessionManager.sharedsessionManager
        self.countryNameFT.text = countryCodesHelper.getDefaultCountry()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countryCodesHelper.countries.count
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        var text = countryCodesHelper.countries[row]
        countryNameFT.text = text
        countryNameFT.resignFirstResponder()
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return countryCodesHelper.countries[row]
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent){
        self.view.endEditing(true)
    }
    

}
