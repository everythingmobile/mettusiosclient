//
//  FeedViewController.swift
//  SMS_VERF
//
//  Created by Pradeep on 20/09/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
import UIKit
class FeedViewController : UITableViewController,UITableViewDelegate,UITableViewDataSource,RouterProtocol{
    var feedPresenter: FeedPresenter?
    var dataSource = FeedDataSource()
    var router: Router?
    var didAnimateCell:[NSIndexPath: Bool] = [:]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "MeetUp Feed"
        self.addMeetUpButton()
        /*var openItem = UIBarButtonItem(title:"O" ,style:UIBarButtonItemStyle.Plain ,target:self ,action:"openButtonPressed")
        self.navigationItem.leftBarButtonItem = openItem;*/
        self.feedPresenter = FeedPresenter()
        self.feedPresenter!.feedManager = FeedManager()
        self.feedPresenter!.sessionManager = SessionManager.sharedsessionManager
        SessionManager.sharedsessionManager.loadUserInfoFromStore()
        self.tableView.dataSource = self.dataSource
        self.tableView.backgroundColor = UIColor(red: 244.0/255, green: 244.0/255, blue: 244.0/255, alpha: 1)
        self.loadUserFeed()
        registerForRefreshNotification()
    }
    
    /*override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return  300
    }*/
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if didAnimateCell[indexPath] == nil || didAnimateCell[indexPath]! == false {
            didAnimateCell[indexPath] = true
            TipInCellAnimator.animate(cell)
        }
    }
    
    func addMeetUpButton(){
        var barButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Add, target: self, action: "createMeetUp")
        self.navigationItem.rightBarButtonItem = barButtonItem
    }
    
    func createMeetUp(){
        router?.segueToCreateMeetUp()
    }
    
    /*func openButtonPressed(){
        self.sideMenuViewController.openMenuAnimated(true, completion: nil)
    }*/
    
    func registerForRefreshNotification(){
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "loadUserFeed", name: "meetUpCreated", object: nil)
    }
    
    func loadUserFeed(){
       feedPresenter?.loadUserFeed({ [unowned self](feed) -> () in
                self.dataSource.meetUpFeed = feed
                self.tableView.reloadData()
            })
    }
    
    override func tableView(tableView:UITableView,didSelectRowAtIndexPath indexPath:NSIndexPath){
        var selectedMeetUp = self.dataSource.meetUpFeed[indexPath.row]
        router?.segueToMeetUpDetail(sender: self, meetUp: selectedMeetUp)
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
}
