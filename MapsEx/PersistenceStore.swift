//
//  PersistenceStore.swift
//  MapsEx
//
//  Created by Pradeep on 09/11/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
class PersistenceStore{
    class var sharedPersistenceStore : PersistenceStore {
        
    struct Static {
        static var instance : PersistenceStore?
        static var token: dispatch_once_t = 0
        }
        
        dispatch_once(&Static.token) {
            Static.instance = PersistenceStore()
            
        }
        return Static.instance!
    }
    
    func persistUserInfo( userInfo : UserInfo) {
        let nsUserDefaults = NSUserDefaults.standardUserDefaults()
        nsUserDefaults.setObject(userInfo.userName,forKey: "userName")
        nsUserDefaults.setObject(userInfo.phoneInfo.phoneNumber,forKey: "phoneNumber")
        nsUserDefaults.setObject(userInfo.phoneInfo.countryCode,forKey: "countryCode")
        nsUserDefaults.synchronize()
    }
    
    func getUserInfo() -> UserInfo{
        let nsUserDefaults = NSUserDefaults.standardUserDefaults()
        var userName = ""
        if let name = nsUserDefaults.objectForKey("userName") as? String{
            userName  = name
        }
        var phoneNumber = nsUserDefaults.objectForKey("phoneNumber") as! String
        var countryCode = nsUserDefaults.objectForKey("countryCode") as! String
        var userInfo = UserInfo()
        userInfo.userName = userName
        userInfo.phoneInfo = PhoneInfo(countryCode: countryCode,phoneNumber: phoneNumber)
        return userInfo
    }
    
    func persistAuthToken(token : String) {
        let nsUserDefaults = NSUserDefaults.standardUserDefaults()
        nsUserDefaults.setObject(token, forKey: "authToken")
        nsUserDefaults.synchronize()
    }
    
    func getAuthToken()->  String? {
        let nsUserDefaults = NSUserDefaults.standardUserDefaults()
        var authToken: String? = nsUserDefaults.objectForKey("authToken") as! String?
        return authToken
    }
    
    func persistFriends(friends : Array<Dictionary<String,String>>) {
        let nsUserDefaults = NSUserDefaults.standardUserDefaults()
        nsUserDefaults.setObject(friends, forKey: "friends")
        nsUserDefaults.synchronize()
    }
    
    func persistIsLoggedInFlag(){
        let nsUserDefaults = NSUserDefaults.standardUserDefaults()
        nsUserDefaults.setObject(true, forKey: "IsLoggedIn")
        nsUserDefaults.synchronize()
    }
    
    func clearAllSavedResources(){
        let nsUserDefaults = NSUserDefaults.standardUserDefaults()
        let appDomain = NSBundle.mainBundle().bundleIdentifier
        nsUserDefaults.removePersistentDomainForName(appDomain!)
    }

}