//
//  ContactViewCell.swift
//  MapsEx
//
//  Created by Pradeep on 02/11/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
class ContactViewCell: UITableViewCell{
    @IBOutlet weak var contactName: UILabel!
    @IBOutlet weak var contactNumber: UILabel!
}