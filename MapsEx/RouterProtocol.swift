//
//  RouterProtocol.swift
//  MapsEx
//
//  Created by Pradeep on 30/11/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
@objc protocol RouterProtocol{
    var router: Router?{get set}
}
