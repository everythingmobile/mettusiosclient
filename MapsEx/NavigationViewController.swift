//
//  NavigationViewController.swift
//  MapsEx
//
//  Created by Pradeep on 04/11/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//
import  UIKit
import Foundation
class NavigationViewController: UIViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let destinationViewController = segue.destinationViewController as MMDrawerController
        
        // Instantitate and set the center view controller.
        let centerViewController = self.storyboard.instantiateViewControllerWithIdentifier("feedBoard") as UIViewController
        destinationViewController.centerViewController = centerViewController
        
        // Instantiate and set the left drawer controller.
        let leftDrawerViewController = self.storyboard.instantiateViewControllerWithIdentifier("SIDE_BAR") as UIViewController
        destinationViewController.leftDrawerViewController = leftDrawerViewController
    }

}