//
//  PhoneInfo.swift
//  MapsEx
//
//  Created by Pradeep on 16/11/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
class PhoneInfo: Equatable{
    var countryCode: String
    var phoneNumber: String
    
    init(countryCode cc:String,phoneNumber:String){
        self.countryCode = cc
        self.phoneNumber = phoneNumber
    }
    
    init(){
        countryCode = ""
        phoneNumber = ""
    }
    
    func toDict () -> Dictionary<String,AnyObject> {
        var dict =  Dictionary<String,AnyObject>()
        dict["countryCode"] = self.countryCode
        dict["phoneNumber"] = self.phoneNumber
        return dict
    }
    
    init(fromDictionary dict : NSDictionary){
        countryCode = (dict["countryCode"] as! NSNumber).stringValue
        phoneNumber = (dict["phoneNumber"] as! NSNumber).stringValue
    }
}
func == (lhs: PhoneInfo, rhs: PhoneInfo) -> Bool {
    return lhs.countryCode == rhs.countryCode && lhs.phoneNumber == rhs.phoneNumber
}
