//
//  GeocodedObject.swift
//  MapsEx
//
//  Created by Pradeep on 09/11/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
class GeocodedObject{
    var formatted_address: String = ""
    var name: String = ""
    var location: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 0, longitude: 0)
    
    init(fromDictionary dict: NSDictionary){
        self.formatted_address = dict["formatted_address"] as! String
        self.name = dict["name"] as! String
        let geometry = dict["geometry"] as! NSDictionary
        let location = geometry["location"] as! NSDictionary
        let lat = location["lat"] as! Double
        let lng  = location["lng"] as! Double
        self.location = CLLocationCoordinate2D(latitude: lat, longitude: lng)
    }
    
    init(){
        
    }
}