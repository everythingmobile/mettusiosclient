//
//  CountryCode.swift
//  PickerEx
//
//  Created by Pradeep on 23/11/14.
//  Copyright (c) 2014 Pradeep. All rights reserved.
//

import Foundation
class CountryCode{
    var name: String
    var dial_code: String
    var code: String
    
    init(){
        name = ""
        dial_code = ""
        code = ""
    }
    
    init(fromDictionary dict: NSDictionary){
        name = ""
        code = ""
        dial_code = ""
        if let
            name = dict["name"] as? String,
            code =  dict["code"] as? String
        {
            self.name = name
            self.code = code
        }
        if let  dc = dict["dial_code"] as? NSString{
            dial_code = dc.stringByReplacingOccurrencesOfString("+", withString: "")
        }
    }
}