//
//  CountryCodesHelper.swift
//  MapsEx
//
//  Created by Pradeep on 29/11/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
class CountryCodesHelper{
    var countries  = [String]()
    var countryCodes = Dictionary<String,CountryCode>()
    var countryCodeNames = Dictionary<String,String>()
    
    func getDefaultCountry() -> String{
        if let code = NSLocale.currentLocale().objectForKey(NSLocaleCountryCode) as? String {
            if(countryCodeNames[code] != nil){
                return countryCodeNames[code]!
            }
            return ""
        }
        return ""
    }
    
    func getCountryCodes(){
        var filePath = NSBundle.mainBundle().pathForResource("CountryCodes", ofType: "json")
        var data = NSData(contentsOfFile: filePath!)
        var array = NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers, error: nil) as! NSArray
        for element in array {
            var cc = CountryCode(fromDictionary: element as! NSDictionary)
            countryCodes[cc.name] = cc
            countryCodeNames[cc.code] = cc.name
            countries.append(cc.name)
        }
        countries.sort{ $0 < $1 }
    }
    
}