//
//  MeetUpDetailViewController.swift
//  MapsEx
//
//  Created by Pradeep on 13/12/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import UIKit

class MeetUpDetailViewController: UIViewController,RouterProtocol,UITableViewDataSource,UITextFieldDelegate{
    
    @IBOutlet weak var meetUpLocationAddressLabel: UILabel!
    @IBOutlet weak var meetUpLocationLabel: UILabel!
    @IBOutlet weak var meetUpStaticMapImageView: UIImageView!
    @IBOutlet weak var countOfParticipants: UIButton!
    @IBOutlet weak var meetUpDateTextField: UITextField!

    @IBAction func startMeetUp(sender: AnyObject) {
        if(meetUp != nil) {
            router?.segueToMeetUpMap(meetUp!)
        }
    }
    
    @IBAction func viewComments(sender: AnyObject) {
        if(meetUp != nil){
            router?.segueToUserComments(sender: self, meetUp: meetUp!)
        }
    }
    
    var meetUp: MeetUpEntity?
    var shouldShowInviteButton: Bool = false
    var router: Router?
    var userCommentDataSource = UserCommentDataSource()
    var userCommentPresenter : UserCommentPresenter?
    var meetUpDetailPresenter : MeetUpDetailPresenter?
    var updateMeetUpPresenter: CreateMeetUpsPresenter?
    var newMeetUpDate: NSDate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        meetUpDetailPresenter = getMeetUpDetailPresenter()
        userCommentPresenter =  getUserCommentPresenter()
        updateMeetUpPresenter = getUpdateMeetUpPresenter()
        self.configureImageView()
        self.configureLocationLabel()
        self.configureAddressabel()
        self.configureInviteButton()
        self.configureCountButton()
        self.configureMeetUpDate()
        // Do any additional setup after loading the view.
    }
    
    func getUpdateMeetUpPresenter() -> CreateMeetUpsPresenter{
        let createMeetUpsPresenter = CreateMeetUpsPresenter()
        createMeetUpsPresenter.sessionManager = SessionManager.sharedsessionManager
        createMeetUpsPresenter.meetUpManager = MeetUpManager()
        return createMeetUpsPresenter
    }
    
    func configureCountButton(){
        if(meetUp != nil){
            self.countOfParticipants.setTitle(String(meetUp!.friends.count), forState: UIControlState.Normal)
            self.countOfParticipants.addTarget(self, action: "participantCountButtonPressed", forControlEvents: UIControlEvents.TouchUpInside)
        }
    }
    
    func participantCountButtonPressed(){
        router?.segueToMeetUpParticpants(sender: self, meetUp: meetUp)
    }
    
    func getMeetUpDetailPresenter() -> MeetUpDetailPresenter{
        var meetUpDetailPresenter = MeetUpDetailPresenter()
        meetUpDetailPresenter.sessionManager = SessionManager.sharedsessionManager
        return meetUpDetailPresenter
    }
    
    func configureMeetUpDate(){
        if(meetUp != nil ){
            meetUpDateTextField.text = meetUpDetailPresenter!.getFormattedDate(meetUp)
        }
        meetUpDateTextField.inputView = getDatePickerView()
        meetUpDateTextField.delegate = self
    }
    
    
    func getUserCommentPresenter() -> UserCommentPresenter{
        let ucpresenter = UserCommentPresenter()
        ucpresenter.userCommentManager =  UserCommentManager()
        ucpresenter.sessionManager = SessionManager.sharedsessionManager
        return ucpresenter
    }
    
    func getDatePickerView() -> UIDatePicker{
        var meetUpDatePicker = UIDatePicker()
        meetUpDatePicker.addTarget(self, action: "meetUpDateChanged:", forControlEvents: UIControlEvents.ValueChanged)
        if(meetUp != nil){
            meetUpDatePicker.date = meetUp!.startUpTime
            newMeetUpDate = meetUp!.startUpTime
        }
        return meetUpDatePicker
    }
    
    func meetUpDateChanged(datePicker: UIDatePicker){
        self.meetUpDateTextField.text = meetUpDetailPresenter!.getformatDateFromNSDate(datePicker.date,zone: "")
        newMeetUpDate = datePicker.date
    }
    
  
    
    func configureInviteButton(){
        if(shouldShowInviteButton == true || (meetUp != nil && meetUpDetailPresenter!.isCurrentUserAdmin(meetUp!.adminPhoneInfo) == true )){
            //var inviteButton = UIBarButtonItem(title: "Invite", style: UIBarButtonItemStyle.Plain, target: self, action: "inviteContacts")
            var inviteButton = UIBarButtonItem(image: UIImage(named: "group"), style: UIBarButtonItemStyle.Plain, target: self, action: "inviteContacts")
            self.navigationItem.rightBarButtonItem = inviteButton
        }
    }
    
    
    func inviteContacts(){
        if(meetUp != nil){
            var meetUpEntityChanges = MeetUpEntityChanges()
            meetUpEntityChanges.meetUpId =  meetUp!.meetUpId
            router?.segueToAddContactsToInvite(sender: self,meetUpEntityChanges: meetUpEntityChanges)
        }
    }
    
    func configureLocationLabel(){
        if(self.meetUp != nil){
            self.meetUpLocationLabel.lineBreakMode = NSLineBreakMode.ByWordWrapping
            self.meetUpLocationLabel.numberOfLines = 0
            self.meetUpLocationLabel.text = self.meetUp!.meetUpLocation.locationDescription
            self.meetUpLocationLabel.sizeToFit()
        }
    }
    
    func configureAddressabel(){
            //self.meetUpDateLabel.text = meetUpDetailPresenter!.getFormattedDate(meetUp)
        self.meetUpLocationAddressLabel.lineBreakMode = NSLineBreakMode.ByWordWrapping
        self.meetUpLocationAddressLabel.numberOfLines = 0
        self.meetUpLocationAddressLabel.text = meetUp!.meetUpLocation.locationAddress
        self.meetUpLocationAddressLabel.sizeToFit()
    }
        
    func configureImageView(){
        meetUpDetailPresenter?.getImage(meetUp, completionHandler: { (image) -> () in
            self.meetUpStaticMapImageView.image = image
        })
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        super.touchesBegan(touches, withEvent: event)
        self.view.endEditing(true)
        updateMeetUp()
    }
    
    func updateMeetUp(){
        if(meetUp != nil && newMeetUpDate != nil){
            if(meetUp!.startUpTime != newMeetUpDate!){
                var meetUpEntityChanges = MeetUpEntityChanges()
                meetUpEntityChanges.meetUpId = meetUp!.meetUpId
                meetUpEntityChanges.startUpTime = newMeetUpDate
                updateMeetUpPresenter!.updateMeetup(meetUpEntityChanges)
            }
        }
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        if(meetUp != nil){
            return self.meetUpDetailPresenter!.isCurrentUserAdmin(meetUp!.adminPhoneInfo)
        }
        return false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.meetUp == nil){
            return 0
        }
        return self.meetUp!.friends.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell? = tableView.dequeueReusableCellWithIdentifier("cell") as? UITableViewCell
        if(cell == nil){
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
            cell!.selectionStyle = UITableViewCellSelectionStyle.None
        }
        cell!.textLabel!.text = self.meetUp!.participants[self.meetUp!.friends[indexPath.row]].userName
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
