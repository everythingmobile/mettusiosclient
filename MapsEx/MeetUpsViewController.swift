//
//  MeetUpsViewController.swift
//  MapsEx
//
//  Created by Pradeep on 08/11/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
import UIKit
class MeetUpsViewController: UITableViewController,UITableViewDelegate,RouterProtocol{
    var router: Router?
    var meetUpsPresenter: MeetUpsPresenter?
    var dataSource = FeedDataSource()
    var didAnimateCell:[NSIndexPath: Bool] = [:]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "My MeetUps"
        self.meetUpsPresenter = MeetUpsPresenter()
        self.meetUpsPresenter!.feedManager = FeedManager()
        self.meetUpsPresenter!.sessionManager = SessionManager.sharedsessionManager
        self.tableView.dataSource = self.dataSource
        self.tableView.backgroundColor = UIColor(red: 244.0/255, green: 244.0/255, blue: 244.0/255, alpha: 1)//UIColor.lightGrayColor()
        self.loadUserMeetUps()
        registerForRefreshNotification()
        self.addMeetUpButton()
    }
    
    func addMeetUpButton(){
        var barButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Add, target: self, action: "createMeetUp")
        self.navigationItem.rightBarButtonItem = barButtonItem
    }
    
    func createMeetUp(){
        router?.segueToCreateMeetUp()
    }
    
    func registerForRefreshNotification(){
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "loadUserMeetUps", name: "meetUpCreated", object: nil)
    }
    
    func loadUserMeetUps(){
            println("gete")
            meetUpsPresenter?.loadUserMeetUps({ [unowned self](feed) -> () in
                self.dataSource.meetUpFeed = feed
                self.tableView.reloadData()
            })
    }
    
    override func tableView(tableView:UITableView,didSelectRowAtIndexPath indexPath:NSIndexPath){
        var selectedMeetUp = self.dataSource.meetUpFeed[indexPath.row]
        router?.segueToMeetUpDetail(sender: self, meetUp: selectedMeetUp, isMyMeetUp: true)
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if didAnimateCell[indexPath] == nil || didAnimateCell[indexPath]! == false {
            didAnimateCell[indexPath] = true
            TipInCellAnimator.animate(cell)
        }
    }
    
    deinit{
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
}