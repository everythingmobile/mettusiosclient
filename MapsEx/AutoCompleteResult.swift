//
//  AutoCompleteResult.swift
//  MapsEx
//
//  Created by Pradeep on 28/12/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
class AutoCompleteResult {
    var description: String
    var place_id: String
    
    init(description: String,place_id: String){
        self.description = description
        self.place_id = place_id
    }
    
    init(){
        description = ""
        place_id = ""
    }
}
