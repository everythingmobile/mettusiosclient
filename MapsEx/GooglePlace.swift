//
//  GooglePlace.swift
//  Feed Me
//
//  Created by Ron Kliffer on 8/30/14.
//  Copyright (c) 2014 Ron Kliffer. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation

class GooglePlace {
  
  let name: String
  let address: String
  let coordinate: CLLocationCoordinate2D
  var photoReference: String?
  var photo: UIImage?
  var place_id : String
  
  init(dictionary:NSDictionary){
    name = dictionary["name"] as! String
    address = dictionary["vicinity"] as! String
    
    let location = dictionary["geometry"]?["location"] as! NSDictionary
    let lat = location["lat"] as! CLLocationDegrees
    let lng = location["lng"]as! CLLocationDegrees
    coordinate = CLLocationCoordinate2DMake(lat, lng)
    place_id  = dictionary["place_id"] as! String
    
    if let photos = dictionary["photos"] as? NSArray {
      let photo = photos.firstObject as! NSDictionary
      photoReference = photo["photo_reference"] as? String
    }

    }
}
