//
//  UserAndUserComment.swift
//  MapsEx
//
//  Created by Pradeep on 01/02/15.
//  Copyright (c) 2015 pradeep. All rights reserved.
//

import Foundation
class UserAndUserComment{
    var userInfo : UserInfo
    var userComment : UserComment
    
    init(fromDictionary dict: [String : AnyObject]){
        if let userDict = dict["userInfo"] as? [String:AnyObject]{
            userInfo = UserInfo(fromDictionary: userDict)
        }
        else{
             userInfo = UserInfo()
        }
        
        if let userCommentDict = dict["userComment"] as? [String:AnyObject]{
            userComment = UserComment(fromDictionary: userCommentDict)
        }
        else{
            userComment = UserComment()
        }
    }
}
