//
//  MeetUpManager.swift
//  MapsEx
//
//  Created by Pradeep on 06/12/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import UIKit

class MeetUpManager {
    var baseUrl = Constants.baseUrl
    
    func createMeetUp(startMeetUpMessage: StartMeetUpMessage){
         request(.POST, baseUrl+"addMeetUp", parameters: startMeetUpMessage.toDict(), encoding: .JSON).responseString { (request, response, string, error) -> Void in
            if(error != nil || string == nil){
                println("couldnt create meetup")
            }
            else{
                println(string!)
                NSNotificationCenter.defaultCenter().postNotificationName("meetUpCreated", object: nil)
            }
        }
    }
    
    func inviteContacts(inviteMessage: InviteMessage){
        request(.POST, baseUrl+"invite", parameters: inviteMessage.toDict(), encoding: .JSON).responseString { (request, response, str, error) -> Void in
            println(response)
            if(error == nil){
                NSNotificationCenter.defaultCenter().postNotificationName("meetUpCreated", object: nil)
            }
        }
    }
    
    func updateMeetUp(meetUpEntityChanges: MeetUpEntityChanges){
        request(.POST, baseUrl+"updateMeetUp", parameters: meetUpEntityChanges.toDict(), encoding: .JSON).responseString { (request, response, str, error) -> Void in
            if(error == nil){
                NSNotificationCenter.defaultCenter().postNotificationName("meetUpCreated", object: nil)
            }
        }
    }
}
