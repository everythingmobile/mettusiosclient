//
//  UserComment.swift
//  MapsEx
//
//  Created by Pradeep on 25/01/15.
//  Copyright (c) 2015 pradeep. All rights reserved.
//

import Foundation
internal class UserComment{
    var commentId : Int
    var meetUpId : String
    var phoneInfo : PhoneInfo
    var comment : String
    var commentTime : NSDate
    
    init(){
        commentId = 0
        meetUpId = ""
        phoneInfo = PhoneInfo()
        comment = ""
        commentTime = NSDate()
    }
    
    init(fromDictionary dict: NSDictionary){
        if let commentId = dict["commentId"] as? Int{
            self.commentId = commentId
        }
        else{
            self.commentId = 0
        }
        
        if let meetUpId = dict["meetUpId"] as? String{
            self.meetUpId = meetUpId
        }
        else{
            self.meetUpId = ""
        }
        
        if let phoneInfo =  dict["phoneInfo"] as? NSDictionary {
            self.phoneInfo = PhoneInfo(fromDictionary: phoneInfo )
        }
        else{
            self.phoneInfo = PhoneInfo()
        }
        
        if let comment = dict["comment"] as? String{
            self.comment = comment
        }
        else{
            self.comment = ""
        }
        
        if let date  = dict["commentTime"] as? String {
            self.commentTime = UserComment.dateFromString(date)
        }
        else{
            self.commentTime = NSDate()
        }
    }
    
    func toDict() -> Dictionary<String,AnyObject> {
        var dict : Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dict["commentId"] = commentId
        dict["meetUpId"] = meetUpId
        dict["phoneInfo"] = phoneInfo.toDict()
        dict["commentTime"] = dateToString()
        dict["comment"] = comment
        return dict
    }
    
    func dateToString() -> String {
        var nsDateFormatter = NSDateFormatter()
        nsDateFormatter.dateFormat = "MMM d, yyyy h:mm:ss a"
        return nsDateFormatter.stringFromDate(commentTime)
    }
    
    class func dateFromString(date: String) -> NSDate{
        var nsDateFormatter = NSDateFormatter()
        nsDateFormatter.dateFormat = "MMM d, yyyy h:mm:ss a"
        nsDateFormatter.timeZone = NSTimeZone(name: "GMT")
        return nsDateFormatter.dateFromString(date)!
    }
}