//
//  MainQueueHelper.swift
//  MapsEx
//
//  Created by Pradeep on 14/12/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
func MQ(operation: () -> () ) {
    NSOperationQueue.mainQueue().addOperationWithBlock { () -> Void in
        operation()
    }
}