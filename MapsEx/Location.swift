//
//  Location.swift
//  MapsEx
//
//  Created by Pradeep on 16/11/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
class Location{
    var destinationLatitude: String
    var destinationLongitude: String
    var locationDescription: String
    var locationAddress: String

    init(){
        self.destinationLatitude = ""
        self.destinationLongitude = ""
        self.locationDescription = ""
        self.locationAddress = ""
    }
    
    func toDict()-> Dictionary<String,AnyObject>{
        var dict = Dictionary<String,AnyObject>()
        dict["destinationLatitude"] = destinationLatitude
        dict["destinationLongitude"] = destinationLongitude
        dict["locationDescription"] = locationDescription
        dict["locationAddress"] = locationAddress
        return dict
    }
    
    init(fromDictionary dict: NSDictionary){
        destinationLatitude = dict["destinationLatitude"] as! String
        destinationLongitude = dict["destinationLongitude"] as! String
        locationDescription = dict["locationDescription"] as! String
        if let loc = dict["locationAddress"] as? String{
            locationAddress = loc
        }
        else {
            locationAddress = ""
        }
    }
}
