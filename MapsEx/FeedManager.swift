//
//  FeedManager.swift
//  MapsEx
//
//  Created by Pradeep on 08/11/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
class FeedManager{
   var baseUrl = Constants.baseUrl
   
    func getUserFeed(userMeetUpsRequest : UserMeetUpsRequest, completionHandler: (feed: Array<MeetUpEntity>)->())  {
        let req = request(.GET, baseUrl+"getUserFeed", parameters:userMeetUpsRequest.toDict())
        req.responseJSON { (request, response, data, error) -> Void in
            if (error == nil) {
                //println("Data :\n \(data)")
                if let arrayOfFeed = data as? NSArray{
                    var feed = self.createMeetUpFeed(fromArray: arrayOfFeed)
                    //println(feed)
                    NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                        completionHandler(feed: feed)
                    })
                }
            }
        }
    }
    
    func getUserMeetUps(userMeetUpsRequest : UserMeetUpsRequest, completionHandler: (feed: Array<MeetUpEntity>)->())  {
        let req = request(.GET, baseUrl+"getUserMeetUps", parameters:userMeetUpsRequest.toDict())
        req.responseJSON {[unowned self] (request, response, data, error) -> Void in
            if (error == nil) {
                //println("Data :\n \(data)")
                if let arrayOfFeed = data as? NSArray{
                    var feed = self.createMeetUpFeed(fromArray: arrayOfFeed)
                    //println(feed)
                    NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                        completionHandler(feed: feed)
                    })
                }
            }
        }
    }
    
    func createMeetUpFeed(fromArray arrayOfFeed : NSArray) -> Array<MeetUpEntity>{
        var meetUpFeedList = Array<MeetUpEntity>()
        for feedObject in arrayOfFeed{
            if let feed = feedObject as? NSDictionary{
                var meetUpEntity = MeetUpEntity(fromDictionary: feed)
                meetUpFeedList.append(meetUpEntity)
            }
        }
        return meetUpFeedList
    }
   
}