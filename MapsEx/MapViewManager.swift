//
//  MapViewManager.swift
//  MapsEx
//
//  Created by Pradeep on 19/10/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
class MapViewManager: NSObject,GMSMapViewDelegate{
    var gmsMapView = GMSMapView()
    var meetUp: MeetUpEntity?
    var meetupMemebers:Dictionary<String,GMSMarker> = Dictionary<String,GMSMarker>()
    var meetupMemberPrevLoc: Dictionary<String,CLLocationCoordinate2D> = Dictionary<String,CLLocationCoordinate2D>()
    var meetupMemberPathColor : Dictionary<String,UIColor> = Dictionary<String,UIColor>()
    var id:String?
    var colors = [UIColor.blackColor(),UIColor.blueColor(),UIColor.redColor(),UIColor.greenColor()]
    var locationChangeHandler : LocationChangedHanler?
    
    func createAndConfigureMapView()->GMSMapView{
        var mapView = self.gmsMapView
        mapView.delegate = self
        configureMapView(mapView)
        return mapView
    }
    
    func configureMapView(mapView : GMSMapView){
        mapView.myLocationEnabled=true
        mapView.settings.myLocationButton=true
        mapView.settings.compassButton=true
        var location=mapView.myLocation
        if (location != nil){
            println("cur loc")
            mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 14, bearing: 0, viewingAngle: 0)
            mapView.animateToCameraPosition(GMSCameraPosition(target: location.coordinate, zoom: 14, bearing: 0, viewingAngle: 0))
        }
    }
    
    func addObserverForMap(mapView : GMSMapView? ){
       if(mapView != nil){
            mapView?.addObserver(self, forKeyPath: "myLocation", options: NSKeyValueObservingOptions.New, context: nil)
            println("observer added")
        }
    }
    
    func removeObserverForMap(mapView: GMSMapView?){
        if(mapView != nil){
             mapView?.removeObserver(self, forKeyPath: "myLocation")
            println("observer removed")
        }
    }
    
    func mapView(mapView: GMSMapView!, didTapMarker marker: GMSMarker!) -> Bool {
        println("Inside didTapMarker method")
        if mapView.myLocation != nil {
            var url = "http://maps.googleapis.com/maps/api/directions/json"
            var originLat = mapView.myLocation.coordinate.latitude
            var originLon = mapView.myLocation.coordinate.longitude
            var destLat = marker.position.latitude
            var destLon = marker.position.longitude
            var sensor = true
            var key = ""
            var dict : Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
            dict["origin"] = "\(originLat),\(originLon)"
            dict["originLon"] = "\(destLat),\(destLon)"
            dict["sensor"] = sensor
            dict["key"] = key
            request(.GET, url, parameters: dict, encoding: .JSON).responseJSON { [unowned self](req, res, json, error) -> Void in
                if error == nil {
                    println(json)
                } else {
                    println(error)
                }
            }
        }
        return true
    }
    
    override func observeValueForKeyPath(keyPath: String, ofObject object: AnyObject?, change: [NSObject : AnyObject], context: UnsafeMutablePointer<Void>){
        var myMap = object as? GMSMapView
        if  (myMap != nil){
            
            /* self.myMap!.animateToCameraPosition(GMSCameraPosition.cameraWithLatitude(self.myMap!.myLocation.coordinate.latitude, longitude: self.myMap!.myLocation.coordinate.longitude, zoom: self.myMap!.camera.zoom))*/
            if let id=self.id{
                
                var lat =  myMap?.myLocation.coordinate.latitude
                var long  =  myMap?.myLocation.coordinate.longitude
                var message = Dictionary<String,String>()
                message["id"] = id
                if (lat != nil && long != nil){
                    message["latitude"] = "\(lat!)"
                    message["longitude"] = "\(long!)"
                   // webSocketManager.sendMessage(message)
                locationChangeHandler?.handleLocationChange(message)
                }
            }
        }
        // locationChangeHandler?.handleLocationChange()
    }
    
    
    func updateMarkersForData(mapView: GMSMapView, data:Dictionary<String,String>){
        var id=data["id"]
        if (id != nil  /*&& id != self.id */)  {
            var marker : GMSMarker? = meetupMemebers[id!]
            var lat = NSString(string:  data["latitude"]!).doubleValue
            var long = NSString(string:data["longitude"]!).doubleValue
            if (marker == nil )  {
                marker=GMSMarker(position: CLLocationCoordinate2DMake(lat, long))
                self.meetupMemebers[id!] = marker!
                marker!.map = mapView
                marker?.title = id!
            }
            else if (marker != nil) {
                
                marker!.position = CLLocationCoordinate2DMake(lat, long)
                marker!.map = mapView
                marker?.title = id!
            }
            drawPolyLine(mapView,lat: lat, longi: long,id: id!)
            
        }
    }
    
    func drawPolyLine(mapView: GMSMapView, lat : Double,longi : Double, id : String ){
        var prevLoc = meetupMemberPrevLoc[id]
        var pathColor = meetupMemberPathColor[id]
        if(pathColor == nil){
            if let color = self.colors.last{
                pathColor = color
                colors.removeLast()
            }else{
                pathColor = UIColor.grayColor()
            }
            meetupMemberPathColor[id]=pathColor!
        }
        var gmsMutablePath = GMSMutablePath()
        if(prevLoc != nil){
            gmsMutablePath.addCoordinate(prevLoc!)
            gmsMutablePath.addCoordinate(CLLocationCoordinate2DMake(lat, longi))
        }
        meetupMemberPrevLoc[id] = CLLocationCoordinate2DMake(lat, longi)
        var polyLine = GMSPolyline(path: gmsMutablePath)
        polyLine.strokeColor = pathColor!
        polyLine.strokeWidth = 4.0
        polyLine.map = mapView
    }
}