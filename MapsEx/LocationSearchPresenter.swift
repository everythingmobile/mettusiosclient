//
//  LocationSearchPresenter.swift
//  MapsEx
//
//  Created by Pradeep on 09/11/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
class LocationSearchPresenter{
    init(){
        
    }
    var locationSearchManager: LocationSearchManager?
    func searchPlace(var place: String,completionHandler:([AutoCompleteResult])->()){
        locationSearchManager?.searchPlace(place, completionHandler: completionHandler)
    }
    
    func geocodeLocation(var place: String,onGeocodingComplete: (GeocodedObject)->()){
        locationSearchManager?.geocodeLocation(place,onGeocodingComplete: onGeocodingComplete)
    }
    
    func getLocation(fromAutoCompleteResult autoCompleteResult: AutoCompleteResult,onRetreiveingLocation: (GeocodedObject) -> ()){
        locationSearchManager?.getLocation(fromAutoCompleteResult: autoCompleteResult,onRetreiveingLocation: onRetreiveingLocation)
    }
}