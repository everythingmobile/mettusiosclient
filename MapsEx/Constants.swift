//
//  Constants.swift
//  MapsEx
//
//  Created by Pradeep on 28/02/15.
//  Copyright (c) 2015 pradeep. All rights reserved.
//

import Foundation
class Constants {
    class var baseUrl: String {
        get{
            return "http://ec2-52-74-140-186.ap-southeast-1.compute.amazonaws.com:8080/MeetusServer/"
        }
    }
    class var goServerUrl: String {
        get{
            return "ws://ec2-52-74-140-186.ap-southeast-1.compute.amazonaws.com:4000/ws"
        }
    }
}
