//
//  WebSocketManager.swift
//  MapsEx
//
//  Created by Pradeep on 19/10/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
class WebSocketManager: NSObject,SRWebSocketDelegate{
    var meetUp: MeetUpEntity?
    var phoneInfo: PhoneInfo?
    var authToken: String?
    var sessionManager: SessionManager?
    var webSocket:SRWebSocket?
    var webSocketMessageReceiver :WebSocketMessageReceiver?
    let connectionUrl = Constants.goServerUrl
    var isSocketOpen=false
    
    func connectToHub() {
        if (webSocket == nil) {
            webSocket=SRWebSocket(URL: NSURL(string: connectionUrl))
        }
        if (webSocket != nil) {
            webSocket!.delegate = self
            webSocket!.open()
        }
    }
    
    func webSocketDidOpen(webSocket: SRWebSocket!) {
        println("Opened Connection")
        var initMessage=["MessageHead":"init","MeetUpId": meetUp!.meetUpId,"UserPhoneNumber": phoneInfo!.phoneNumber.toInt()!,"UserCountryCode": phoneInfo!.countryCode.toInt()!,"UserToken": authToken!]
        var error:NSError?
        var data=NSJSONSerialization.dataWithJSONObject(initMessage, options: NSJSONWritingOptions.PrettyPrinted, error: &error)
        webSocket.send(data)
        isSocketOpen=true
    }
    
    func webSocket(webSocket: SRWebSocket!, didReceiveMessage message: AnyObject!) {
        println(message)
        var error:NSError?
        var data = message as? String
        var jsonData:NSData? = data?.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
        if (jsonData != nil){
            var dict = NSJSONSerialization.JSONObjectWithData(jsonData!, options: NSJSONReadingOptions.MutableContainers, error: &error) as? Dictionary<String,String>
            if (error == nil && dict != nil) {
                webSocketMessageReceiver?.messageReceivedHandler(dict!)
            }
            else{
                println("Error occured while deserializing json")
            }
        }
    }
    
    func webSocket(webSocket: SRWebSocket!,didFailWithError error:NSError!){
        if (self.webSocket != nil){
            self.webSocket!.delegate=nil
            self.webSocket=nil
        }
        self.isSocketOpen = false
    }
    
    func webSocket(webSocket: SRWebSocket!,didCloseWithCode code:NSInteger!,reason :NSString!,wasClean:Bool){
        if (self.webSocket != nil){
            self.webSocket!.delegate=nil
            self.webSocket=nil
        }
         self.isSocketOpen = false
    }
    
    func sendMessage(message:Dictionary<String,String>){
        var error:NSError?
        var data=NSJSONSerialization.dataWithJSONObject(message, options: NSJSONWritingOptions.PrettyPrinted, error: &error)
        if (error == nil && self.webSocket != nil && self.isSocketOpen == true ) {
            self.webSocket!.send(data)
        }
    }
    
}
