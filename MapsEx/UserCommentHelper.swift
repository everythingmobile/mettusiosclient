//
//  UserCommentHelper.swift
//  MapsEx
//
//  Created by Pradeep on 25/01/15.
//  Copyright (c) 2015 pradeep. All rights reserved.
//

import Foundation
class UserCommentHelper {
    var meetUpId :  String = ""
    var countryCode: String = ""
    var phoneNumber: String = ""
    
    func toDict() -> [String : AnyObject]{
        var dict = [String : AnyObject]()
        dict["meetUpId"] = meetUpId
        dict["countryCode"] = countryCode
        dict["phoneNumber"] = phoneNumber
        return dict
    }
    
    init(meetUpId: String, countryCode: String, phoneNumber: String){
        self.meetUpId = meetUpId
        self.countryCode  = countryCode
        self.phoneNumber = phoneNumber
    }
}