//
//  SideMenuNavigationController.swift
//  MapsEx
//
//  Created by Pradeep on 09/11/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
import UIKit
class SideMenuNavigationController: MSDynamicsDrawerViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var msd = self
        
        var router: Router = Router(storyboard: self.storyboard!, msDynamicsViewController: msd)
        var menuController = router.getController("MENU_BOARD") as! MenuViewContoller
        router.setDrawerViewContoller(controller: menuController, forDirection: MSDynamicsDrawerDirection.Left)
        menuController.router = router
        router.setPaneViewController("MeetUp Feed")
        var sessionManager =  SessionManager.sharedsessionManager
        sessionManager.loadUserInfoFromStore()
    }
}