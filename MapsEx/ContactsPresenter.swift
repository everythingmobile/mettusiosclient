//
//  ContactsPresenter.swift
//  MapsEx
//
//  Created by Pradeep on 09/11/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
import AddressBook
class ContactsPresenter{
    init(){
        
    }
    var sessionManager: SessionManager?
    var friends = Array<UserInfo>()
    var contactList = Array<Contacts>()
    var verificationManager: VerificationManger?
    func retrieveFriendContacts(#completionHandler: ([UserInfo])->()) {
        var userInfo: UserInfo = sessionManager!.userSession!.userInfo!
        let status = ABAddressBookGetAuthorizationStatus()
        switch status {
        case .Denied, .Restricted:
            println("User has denied or restricted your app access to Address Book!")
            case .Authorized, .NotDetermined:
            var error : Unmanaged<CFError>? = nil
            var addressBook : ABAddressBook? = ABAddressBookCreateWithOptions(nil, &error).takeRetainedValue()
            if addressBook == nil {
                println(error)
                return
            }
            ABAddressBookRequestAccessWithCompletion(addressBook) {
                (granted:Bool, err:CFError!) in
                if granted {
                    var allContacts:NSArray = ABAddressBookCopyArrayOfAllPeople(addressBook)!.takeUnretainedValue() as NSArray
                    //println(allContacts)
                    
                    for (person: AnyObject) in allContacts {
                        let unmanagedPhones = ABRecordCopyValue(person, kABPersonPhoneProperty)
                        let name: CFString! = ABRecordCopyCompositeName(person)!.takeUnretainedValue()
                        let phones: ABMultiValueRef =
                        Unmanaged.fromOpaque(unmanagedPhones.toOpaque()).takeUnretainedValue()
                            as NSObject as ABMultiValueRef
                        let countOfPhones = ABMultiValueGetCount(phones)
                        //println(countOfPhones)
                        for index in 0..<countOfPhones {
                            let unmanagedPhone = ABMultiValueCopyValueAtIndex(phones, index)
                            let phone: String = Unmanaged.fromOpaque(
                                unmanagedPhone.toOpaque()).takeUnretainedValue() as NSObject as! String
                            if let nm = name {
                                //  print("\(nm) : ")
                                println(nm)
                                let countryCodeAndPhoneNumber = self.getCountryCodeAndNumber(phone)
                                println(countryCodeAndPhoneNumber)
                                if(countryCodeAndPhoneNumber.phoneNumber != ""){
                                    var contact: Contacts = Contacts(name: nm as String, phoneInfo: PhoneInfo(countryCode: countryCodeAndPhoneNumber.countryCode, phoneNumber: countryCodeAndPhoneNumber.phoneNumber))
                                    self.contactList.append(contact)
                                }
                            }
                            //  println(self.contactList.count)
                        }
                    }
                    self.verifyFriends(userInfo: userInfo,completionHandler: completionHandler)
                }
                else {
                    println(err)
                    println("User has denied your app access to Address book!")
                }
            }
        }
    }
    
    func filterPhoneNumber(phoneNumber: String) -> String {
        var number = phoneNumber;
        number = number.stringByReplacingOccurrencesOfString("[^0-9]", withString: "", options: .RegularExpressionSearch, range:nil)
        /*var char: Character = Array(number)[0]
        if char == "0" {
            number = (number.substringFromIndex(advance(number.startIndex, 1)))
            number = "91" + number;
        }*/
        //println(number)
        return number;
    }
    
    func getCountryCodeAndNumber(phoneNumber: String) -> (countryCode: String,phoneNumber: String) {
        let phone = filterPhoneNumber(phoneNumber)
        let phoneUtil = NBPhoneNumberUtil.sharedInstance()
        var nationalNumber: NSString?
        var countryCode = phoneUtil.extractCountryCode(phone, nationalNumber: &nationalNumber)
        if(nationalNumber == nil || countryCode == 0 ){
            return ("","")
        }
        else{
            return (countryCode.stringValue, filterPhoneNumber(nationalNumber as String!))
        }
    }
    
    func verifyFriends (#userInfo: UserInfo,completionHandler:[UserInfo]->() ) {
        //sleep(3)
        var userAndContacts =  UserAndContacts()
        //println("Important : ",self.contactList.count)
        userAndContacts.userInfo = userInfo
        for (person : Contacts) in self.contactList {
            var user = UserInfo( userName: person.name, phoneInfo: person.phoneInfo, emailId: " " , registered: false)
            userAndContacts.userContacts.append(user)
        }
        self.verificationManager?.verifyFriends(userAndContacts).responseJSON { [unowned self](req, res, json, error) -> Void in
            if error == nil {
                //println(json)
                var friendList = json as? NSArray
                var friendsInfo = Array<UserInfo>()
                if(friendList != nil){
                    for userObject in friendList!{
                        if let userDict = userObject as? NSDictionary{
                            var userInfo = UserInfo(fromDictionary: userDict)
                            friendsInfo.append(userInfo)
                        }
                    }
                    completionHandler(friendsInfo)
                }
            } else {
                println("error occrd")
                println(error)
            }
        }
    }
}
