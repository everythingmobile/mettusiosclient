//
//  NetworkReachability.swift
//  MapsEx
//
//  Created by Pradeep Borado on 19/04/15.
//  Copyright (c) 2015 pradeep. All rights reserved.
//

import Foundation
class NetworkReachability {
    var internetReachable: Reachability
    
    init(){
        internetReachable = Reachability.reachabilityForInternetConnection()
        internetReachable.startNotifier()
    }
}