//
//  FeedViewCell.swift
//  MapsEx
//
//  Created by Pradeep on 20/10/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
class FeedViewCell: UITableViewCell{
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var subjectLabel: UILabel!
    @IBOutlet weak var mapImage: UIImageView!
    
}
