//
//  RegistrationManager.swift
//  MapsEx
//
//  Created by Pradeep on 08/11/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
class RegistrationManager{
    var baseUrl = Constants.baseUrl
    init(){
        
    }
    func registerUser (userInfo : UserInfo,completionHandler: (error: NSError?)->()) {
        request(.POST, baseUrl+"useradd", parameters:  userInfo.toDict(), encoding: .JSON)
            .responseString {(request, response, string, error) in
                completionHandler(error: error)
        }
    }
}