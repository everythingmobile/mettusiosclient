//
//  Tokens.swift
//  MapsEx
//
//  Created by Pradeep on 23/11/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
class Tokens{
    var phoneInfo: PhoneInfo = PhoneInfo()
    var userName: String = ""
    var authToken: String = ""
    
    init(){
        
    }
    
    init(fromDictionary dict: NSDictionary){
        if let phoneDict = dict["phoneInfo"] as? NSDictionary{
            self.phoneInfo = PhoneInfo(fromDictionary: phoneDict)
        }
        if let userName = dict["userName"] as? String{
            self.userName = userName
        }
        if let authToken = dict["authToken"] as? String{
            self.authToken = authToken
        }
    }
}
