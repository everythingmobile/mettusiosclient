//
//  InviteMessage.swift
//  MapsEx
//
//  Created by Pradeep on 06/12/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import UIKit

class InviteMessage {
   private var meetUpId: String
   private var invitees: [UserInfo]
   private var inviter: UserInfo
    
    init(){
        meetUpId = ""
        invitees = [UserInfo]()
        inviter  = UserInfo()
    }
    
    init(meetUpId: String,invitees: [UserInfo],inviter: UserInfo){
        self.meetUpId = meetUpId
        self.invitees = invitees
        self.inviter = inviter
    }
    
    internal func setMeetUpId(meetUpId: String){
        self.meetUpId = meetUpId
    }
    
    internal func setInvitees(invitees: [UserInfo]){
        self.invitees = invitees
    }
    
    internal func setInviter(inviter: UserInfo){
        self.inviter = inviter
    }
    
    internal func getMeetUpId() -> String{
        return meetUpId
    }
    
    internal func getInvitees() -> [UserInfo]{
        return invitees
    }
    
    internal func getInviter() -> UserInfo{
        return inviter
    }
    
    internal func toDict() -> Dictionary<String,AnyObject> {
        var dict = Dictionary<String,AnyObject>()
        dict["meetUpId"] = meetUpId
        dict["invitees"] = invitees.map{$0.toDict()}
        dict["inviter"] = inviter.toDict()
        return dict
    }
}
