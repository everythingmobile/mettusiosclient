//
//  DummyVC.swift
//  MapsEx
//
//  Created by Pradeep on 13/11/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
import UIKit
class ContactsPickerViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,THContactPickerDelegate,RouterProtocol{
    var rowCount: Int = 5
    var friends = Array<UserInfo>()
    var contactList = Array<Contacts>()
    var privateSelectedList = Array<UserInfo>()
    var filteredFriends = Array<UserInfo>()
    var contactsPresenter: ContactsPresenter?
    var countryCode: String = ""
    var phone : String = ""
    var flag : Int = 0
    var meetUpLocation = Location()
    var router: Router?
    var nextButton: UIButton?
    var startMeetUpMessage = StartMeetUpMessage()
    var meetUpEntityChanges: MeetUpEntityChanges? = nil
    var meetUp: MeetUpEntity?
    var createMeetUpsPresenter: CreateMeetUpsPresenter?
    var nextSegue = NextSegueFromContacts.CreateMeetUp   // default segue
    
    lazy var contactsPicker: THContactPickerView = {
        return THContactPickerView(frame:CGRectMake(0, 0, self.view.frame.size.width, 100.0))
    }()
    
    lazy var tableView: UITableView = {
        var tableFrame = CGRectMake(0, self.contactsPicker.frame.size.height, self.view.frame.size.width, self.view.frame.size.height - self.contactsPicker.frame.size.height);
        return UITableView(frame:tableFrame,style:UITableViewStyle.Plain)
    }()
    
    func configureNextButton(){
        switch nextSegue {
        case .CreateMeetUp:
            if(self.nextButton == nil && meetUpEntityChanges == nil){
              /*  let button   = UIButton.buttonWithType(UIButtonType.System) as UIButton
                button.backgroundColor = UIColor.whiteColor()
                button.frame = CGRectMake(self.view.frame.size.width-130, self.view.frame.size.height-80, 100, 50)
                button.setTitle("Next", forState: UIControlState.Normal)
                button.addTarget(self, action: "segueToChooseMeetUpDate", forControlEvents: UIControlEvents.TouchUpInside)
                self.view.insertSubview(button, aboveSubview: self.tableView)
                self.nextButton = button
                self.nextButton!.hidden = true*/
                
                var nextButton = UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.Plain, target: self, action: "segueToChooseMeetUpDate")
                self.navigationItem.rightBarButtonItem = nextButton
            }
        case .InviteContacts:
            if(meetUpEntityChanges != nil){
                var doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: "doneButtonPressed")
                self.navigationItem.rightBarButtonItem = doneButton
            }
        }
    }
    
    func doneButtonPressed(){
        if(meetUpEntityChanges != nil && self.privateSelectedList.count != 0){
            meetUpEntityChanges!.addedUsers = privateSelectedList
            self.createMeetUpsPresenter?.updateMeetup(meetUpEntityChanges!)
            self.navigationController?.popToRootViewControllerAnimated(true)
        }
        else{
            showAlertMessage(message:"Choose contacts")
        }
    }
    
    func showAlertMessage(#message: String){
        let alertController = UIAlertController(title: message, message: "", preferredStyle: .Alert)
        let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
            // ...
        }
        alertController.addAction(OKAction)
        self.presentViewController(alertController, animated: true,completion: nil)
    }
    
    func segueToChooseMeetUpDate(){
        if(self.privateSelectedList.count != 0){
            startMeetUpMessage.meetUpLocation = meetUpLocation
            startMeetUpMessage.invitees = privateSelectedList
            router?.segueToChooseMeetUpDate(startMeetUpMessage)
        }
        else{
            showAlertMessage(message:"Choose contacts")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardDidShow1:", name: UIKeyboardDidShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardDidHide1:", name: UIKeyboardDidHideNotification, object: nil)
        println("here")
        if (self.respondsToSelector("edgesForExtendedLayout")) {
            self.edgesForExtendedLayout =  UIRectEdge.Bottom|UIRectEdge.Left|UIRectEdge.Right
        }
        self.contactsPicker.autoresizingMask = UIViewAutoresizing.FlexibleBottomMargin|UIViewAutoresizing.FlexibleWidth
        self.contactsPicker.delegate = self
        self.contactsPicker.setPlaceholderLabelText("Who would you like to invite?")
        self.contactsPicker.setPromptLabelText("")
        self.view.addSubview(self.contactsPicker)
        
        var layer = self.contactsPicker.layer
        layer.shadowColor = UIColor(red:225.0/255.0, green:226.0/255.0, blue:228.0/255.0, alpha:1).CGColor
        layer.shadowOffset = CGSizeMake(0, 2)
        layer.shadowOpacity = 1
        layer.shadowRadius = 1.0
        
        self.tableView.autoresizingMask = UIViewAutoresizing.FlexibleHeight | UIViewAutoresizing.FlexibleWidth
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.view.insertSubview(self.tableView, belowSubview: self.contactsPicker)
        
        
        self.contactsPresenter = ContactsPresenter()
        self.contactsPresenter!.sessionManager = SessionManager.sharedsessionManager
        self.contactsPresenter!.verificationManager = VerificationManger()
        SessionManager.sharedsessionManager.loadUserInfoFromStore()
        self.contactsPresenter?.retrieveFriendContacts(completionHandler: {[unowned self] (friends) -> () in
            
            NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                self.friends = friends
                self.filteredFriends = friends
                self.tableView.reloadData()
            })
        })
        
        self.createMeetUpsPresenter = CreateMeetUpsPresenter()
        self.createMeetUpsPresenter!.sessionManager = SessionManager.sharedsessionManager
        self.createMeetUpsPresenter!.meetUpManager = MeetUpManager()
        //verifyFriends()
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidLayoutSubviews() {
        configureNextButton()
        self.adjustTableFrame()
    }
    
    override func viewWillAppear(animated: Bool) {
        //NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardDidShow1", name: UIKeyboardDidShowNotification, object: nil)
        //NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardDidHide1", name: UIKeyboardDidHideNotification, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func viewDidAppear(animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func keyboardDidShow1(notification: NSNotification!) {
        var info: NSDictionary = notification.userInfo! as NSDictionary
        var kbRect: CGRect = self.view.convertRect((info[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue(), fromView: self.view.window)
        self.adjustTableViewInsetBottom(self.tableView.frame.origin.y + self.tableView.frame.size.height - kbRect.origin.y)
    }
    
    func keyboardDidHide1(notification: NSNotification!){
        var info: NSDictionary = notification.userInfo!
        var kbRect: CGRect = self.view.convertRect((info[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue(), fromView: self.view.window)
        self.adjustTableViewInsetBottom(self.tableView.frame.origin.y + self.tableView.frame.size.height - kbRect.origin.y)
    }
    
    func adjustTableViewInsetBottom(bottomInset: CGFloat) {
      self.adjustTableViewInsetTop(self.tableView.contentInset.top,bottom:bottomInset)
    }
    
    func adjustTableViewInsetTop(topInset: CGFloat) {
        self.adjustTableViewInsetTop(topInset,bottom:self.tableView.contentInset.bottom)
    }
    
    func adjustTableViewInsetTop(topInset: CGFloat, bottom bottomInset: CGFloat) {
        self.tableView.contentInset = UIEdgeInsetsMake(topInset,
        self.tableView.contentInset.left,
        bottomInset,
        self.tableView.contentInset.right);
        self.tableView.scrollIndicatorInsets = self.tableView.contentInset;
    }
    
    func adjustTableFrame() {
        let yOffset = self.contactsPicker.frame.origin.y + self.contactsPicker.frame.size.height
        var tableFrame = CGRectMake(0, yOffset, self.view.frame.size.width, self.view.frame.size.height - yOffset)
        self.tableView.frame = tableFrame
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filteredFriends.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell1:UITableViewCell? = self.tableView.dequeueReusableCellWithIdentifier("cell") as? UITableViewCell
        if(cell1 == nil){
            cell1 = UITableViewCell(style:
                UITableViewCellStyle.Default, reuseIdentifier: "cell")
            cell1!.selectionStyle = UITableViewCellSelectionStyle.None
        }
        var cell: UITableViewCell = cell1!
        cell.textLabel!.text = self.filteredFriends[indexPath.row].userName
        //cell.detailTextLabel.text = self.filteredFriends[indexPath.row].phoneInfo.phoneNumber
        if(isPresentInSelectedList(self.filteredFriends[indexPath.row]) >= 0 ){
            cell.accessoryType = UITableViewCellAccessoryType.Checkmark
        }
        else{
             cell.accessoryType = UITableViewCellAccessoryType.None
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var cell = tableView.cellForRowAtIndexPath(indexPath)! as UITableViewCell
        var userInfo = self.filteredFriends[indexPath.row]
        var position = isPresentInSelectedList(userInfo)
        if( position >= 0){
            cell.accessoryType = UITableViewCellAccessoryType.None
            privateSelectedList.removeAtIndex(position)
            contactsPicker.removeContact(userInfo)
        }
        else{
            cell.accessoryType = UITableViewCellAccessoryType.Checkmark
            privateSelectedList.append(userInfo)
            contactsPicker.addContact(userInfo, withName: userInfo.userName)
        }
        filteredFriends = friends
        tableView.reloadData()
        self.showOrHideNextButton()
    }
    
    func showOrHideNextButton(){
      /*  if(self.navigationItem.rightBarButtonItem != nil){
            if(privateSelectedList.count > 0){
               // self.nextButton!.hidden = false
                MQ({ () -> () in
                    self.navigationItem.rightBarButtonItem!.enabled = true
                })
            }
            else{
                //self.nextButton!.hidden = true
                MQ({ () -> () in
                    self.navigationItem.rightBarButtonItem!.enabled = false
                })
            }
        }*/
    }
    
    func isPresentInSelectedList(userInfo: UserInfo) -> Int {
        for (index,user) in enumerate(self.privateSelectedList) {
            if(user.phoneInfo.phoneNumber == userInfo.phoneInfo.phoneNumber && user.phoneInfo.countryCode == userInfo.phoneInfo.countryCode){
                return index
            }
        }
        return -1
    }
    
    func newFilteringPredicateWithText(text: NSString)  -> NSPredicate{
        return NSPredicate(format: "self contains[cd] %\(text)", argumentArray: nil)
    }
    
    func titleForRowAtIndexPath(indexPath: NSIndexPath) -> String {
    return self.filteredFriends[indexPath.row].userName
    }
    
    
    // Contacts picker Delegate Methods
    func contactPickerTextViewDidChange(textViewText: String!) {
        if(textViewText == ""){
            filteredFriends = friends
        }
        else{
            filteredFriends = friends.filter({ (userInfo) -> Bool in
                if(userInfo.userName.lowercaseString.rangeOfString(textViewText) != nil) {
                    return true
                }
                return false
            })
        }
        self.tableView.reloadData()
    }
    
    func contactPickerDidResize(contactPickerView: THContactPickerView!) {
        var frame = self.tableView.frame
        frame.origin.y = contactsPicker.frame.size.height + contactsPicker.frame.origin.y
        self.tableView.frame = frame
    }
    
    func contactPickerDidRemoveContact(contact: AnyObject!) {
        var position = isPresentInSelectedList(contact as! UserInfo)
        if(position >= 0){
            privateSelectedList.removeAtIndex(position)
            self.showOrHideNextButton()
        }
        
        var index = getPositionInArray(friends,userInfo: contact as! UserInfo)
        var cell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: index, inSection: 0))!
        cell.accessoryType = UITableViewCellAccessoryType.None
        
    }
    
    func getPositionInArray(array: [UserInfo],userInfo: UserInfo) -> Int {
        for (index,user) in enumerate(array) {
            if(user.phoneInfo.phoneNumber == userInfo.phoneInfo.phoneNumber && user.phoneInfo.countryCode == userInfo.phoneInfo.countryCode){
                return index
            }
        }
        return -1
    }
    
    func contactPickerTextFieldShouldReturn(textField: UITextField) -> Bool {
        self.resignFirstResponder()
        return true
    }
}