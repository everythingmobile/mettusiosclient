//
//  MenuViewContoller.swift
//  MapsEx
//
//  Created by Pradeep on 12/11/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
import UIKit
import FBSDKCoreKit
import FBSDKShareKit

class MenuViewContoller: UIViewController,UITableViewDelegate,UITableViewDataSource,RouterProtocol,FBSDKAppInviteDialogDelegate{
    @IBOutlet weak var tableView: UITableView!
    var numbers = [1,2,3,4,5]
    var  router: Router?
    var  boards =  ["MeetUp Feed", "My MeetUps", "Location Search", "Contacts", "Logout"]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red: 54.0 / 255.0, green: 215.0 / 255.0, blue: 183.0 / 255.0, alpha: 1)
        self.tableView.backgroundColor = UIColor.clearColor()
        //self.addBlurredImageBackground()
        self.tableView.tableFooterView = UIView(frame: CGRectZero);
    }
    
    func addBlurredImageBackground() {
        let img = UIImage(named: "Flowing River Wallpaper iPhone 6.jpg")
        let bg = img?.applyBlurWithRadius(20, tintColor: UIColor.clearColor(), saturationDeltaFactor: 0.8, maskImage: nil)
        let imageView = UIImageView(image: bg)
        //self.view.insertSubview(imageView, atIndex: 0)
        //self.view.bringSubviewToFront(imageView)
        self.view.insertSubview(imageView, atIndex: 0)
       //self.tableView.backgroundView = imageView
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.boards.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell? = tableView.dequeueReusableCellWithIdentifier("cell") as? UITableViewCell
        if(cell == nil){
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
            cell!.selectionStyle = UITableViewCellSelectionStyle.None
        }
        cell!.backgroundColor = UIColor.clearColor()
        cell!.textLabel!.textColor = UIColor.whiteColor()
        cell!.textLabel!.text = boards[indexPath.row]
        cell!.textLabel!.font = UIFont (name: "Optima", size: 20)
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)! as UITableViewCell
        let board = cell.textLabel!.text! as String
        if(board == "Logout"){
            router?.logOut(sender: self)
        }
        else{
            router?.setPaneViewController(board, animated: true, completion: nil)
        }
        
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 80.0
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        var cell:InviteFriendTableViewCell? = tableView.dequeueReusableCellWithIdentifier("footer") as? InviteFriendTableViewCell
        cell?.facebookInviteButton.addTarget(self, action: "facebookInviteButtonPressed", forControlEvents: UIControlEvents.TouchUpInside)
        cell?.whatsappInvittButton.addTarget(self, action: "whatsappInviteButtonPressed", forControlEvents: UIControlEvents.TouchUpInside)
        return cell
    }
    
    func appInviteDialog(appInviteDialog: FBSDKAppInviteDialog!, didCompleteWithResults results: [NSObject : AnyObject]!) {
        
    }
    
    func appInviteDialog(appInviteDialog: FBSDKAppInviteDialog!, didFailWithError error: NSError!) {
        println(error)
    }
    
    func facebookInviteButtonPressed() {
        println("facebook")
        let inviteContent = FBSDKAppInviteContent()
        inviteContent.appLinkURL = NSURL(string: "https://fb.me/414357588744087")
        inviteContent.previewImageURL = NSURL(string: "http://cache1.asset-cache.net/gc/167652317-two-tourist-girls-on-a-safari-gettyimages.jpg?v=1&c=IWSAsset&k=2&d=qbJeMJN7HeoMHVRKeJCRoyus69tSvYI223AlQoYltsz0stf3BMwMnpXO6R7m7t%2bSmpN2cKJof9cRO7YAYh4SCA%3d%3d")
        FBSDKAppInviteDialog.showWithContent(inviteContent, delegate: self)
    }
    
    func whatsappInviteButtonPressed() {
        println("whatsapp")
        let whatsasppUrl = NSURL(string: "whatsapp://send?text=Hello%2C%20World!")
        if(UIApplication.sharedApplication().canOpenURL(whatsasppUrl!)) {
            UIApplication.sharedApplication().openURL(whatsasppUrl!)
        }
    }
    
}
