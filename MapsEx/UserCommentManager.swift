//
//  UserCommentManager.swift
//  MapsEx
//
//  Created by Pradeep on 25/01/15.
//  Copyright (c) 2015 pradeep. All rights reserved.
//

import Foundation
class UserCommentManager {
    var baseUrl = Constants.baseUrl
    
    func getMeetUpComments(userCommentHelper : UserCommentHelper ,completionHandler : ([UserAndUserComment]) ->()){
        let req = request(.GET, baseUrl+"getMeetUpComments", parameters:userCommentHelper.toDict())
        req.responseJSON { (request, response, data, error) -> Void in
            if (error == nil) {
                if let arrayOfComments = data as? NSArray{
                    var comments = self.createUserCommentList(fromArray: arrayOfComments as [AnyObject])
                    NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                        completionHandler(comments)
                    })
                }
            }
        }
    }
    
    func createUserCommentList(fromArray comments : [AnyObject]) -> [UserAndUserComment]{
        var commentList = [UserAndUserComment]()
        commentList = comments.map({(item)  -> UserAndUserComment in
            var titem = UserAndUserComment(fromDictionary: item as! [String:AnyObject])
            return titem
        })
        return commentList
    }
    
    func addUserComment(userComment : UserComment){
        let req = request(.POST, baseUrl+"addUserComment", parameters: userComment.toDict(), encoding: .JSON)
        req.responseString { (request, response, string, error) -> Void in
            if(error != nil || string == nil){
                println("couldnt add comment")
            }
            else{
                NSNotificationCenter.defaultCenter().postNotificationName("refreshComments", object: nil)
                println(string!)
            }
        }
    }
    
    func updateUserComment(userComment : UserComment){
        let req = request(.POST, baseUrl+"updateUserComment", parameters: userComment.toDict(), encoding: .JSON)
        req.responseString { (request, response, string, error) -> Void in
            if(error != nil || string == nil){
                println("couldnt update comment")
            }
            else{
                println(string!)
                NSNotificationCenter.defaultCenter().postNotificationName("refreshComments", object: nil)
            }
        }
    }
    
    func removeUserComment(userComment : UserComment){
        let req = request(.POST, baseUrl+"removeUserComment", parameters: userComment.toDict(), encoding: .JSON)
        req.responseString { (request, response, string, error) -> Void in
            if(error != nil || string == nil){
                println("couldnt remove comment")
            }
            else{
                println(string!)
                NSNotificationCenter.defaultCenter().postNotificationName("refreshComments", object: nil)
            }
        }
    }
}
