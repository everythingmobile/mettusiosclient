//
//  MeetUpStartTimeSelectionViewController.swift
//  MapsEx
//
//  Created by Pradeep on 06/12/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
import UIKit
class MeetUpStartTimeSelectionViewController: UIViewController,RouterProtocol{
    var startMeetUpMessage = StartMeetUpMessage()
    var createMeetUpsPresenter: CreateMeetUpsPresenter?
    var router: Router?
    
    @IBAction func createMeetUpButtonPressed(sender: UIButton) {
        startMeetUpMessage.startUpTime = meetUpDatePicker.date
        createMeetUpsPresenter?.createMeetUp(startMeetUpMessage)
    }
    
    @IBOutlet weak var meetUpDatePicker: UIDatePicker!
    
    override func viewDidLoad(){
        super.viewDidLoad()
        self.meetUpDatePicker.addTarget(self, action: "meetUpDateChanged", forControlEvents: UIControlEvents.ValueChanged)
        createMeetUpsPresenter = CreateMeetUpsPresenter()
        createMeetUpsPresenter!.meetUpManager = MeetUpManager()
        createMeetUpsPresenter!.sessionManager = SessionManager.sharedsessionManager
    }
    
    func meetUpDateChanged(){
        println(meetUpDatePicker.date)
    }
    
}
