//
//  ViewController.swift
//  MapsEx
//
//  Created by sudheer kumar meesala on 6/29/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import UIKit

class mapViewController: UIViewController,CLLocationManagerDelegate,WebSocketMessageReceiver,LocationChangedHanler {
    var mapViewManager = MapViewManager()
    var webSocketManager = WebSocketManager()
    dynamic var myMap:GMSMapView?
    var locationManager:CLLocationManager?
    var meetUp: MeetUpEntity?
    var router: Router?
    let networkReachability = NetworkReachability()
    var previousInternetStatus: Int = 1
    
    func startSignificantChanges(){
        if locationManager == nil {
            locationManager=CLLocationManager()
        }
        if (locationManager != nil){
            locationManager!.delegate=self
            locationManager!.requestAlwaysAuthorization()
            locationManager!.desiredAccuracy = kCLLocationAccuracyBest
            locationManager!.startUpdatingLocation()
        }
    }
    
    func handleLocationChange(message : Dictionary<String,String>){
        webSocketManager.sendMessage(message)
    }
    
    func messageReceivedHandler(dict : Dictionary<String,String>){
        mapViewManager.updateMarkersForData(self.myMap!,data: dict)
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        
    }
    
    func registerForInternetStatus() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "internetStatusChanged:", name: kReachabilityChangedNotification, object: nil)
    }
    
    func deregisterInternetStatus() {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: kReachabilityChangedNotification, object: nil)
    }
    
    func internetStatusChanged(notification: NSNotification) {
        if(networkReachability.internetReachable.currentReachabilityStatus().value == 0) {
            previousInternetStatus = 0
        } else if(previousInternetStatus == 0 && networkReachability.internetReachable.currentReachabilityStatus().value != 0 ) {
            previousInternetStatus = 1
            webSocketManager.connectToHub()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startSignificantChanges()
        mapViewManager.locationChangeHandler = self
        webSocketManager = self.getWebSocketManager()
        webSocketManager.connectToHub()
        mapViewManager.id = SessionManager.sharedsessionManager.userSession!.userInfo!.userName
        var mapView = mapViewManager.createAndConfigureMapView()
        self.myMap = mapView
        self.title = "MeetUp"
        mapViewManager.addObserverForMap(self.myMap)
        self.view=mapView
        var marker : GMSMarker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake((meetUp!.meetUpLocation.destinationLatitude as NSString).doubleValue,(meetUp!.meetUpLocation.destinationLongitude as NSString).doubleValue)
        marker.title = "Destination"
        marker.map = self.myMap!
        registerForInternetStatus()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func getWebSocketManager() -> WebSocketManager {
        var webSocketManager = WebSocketManager()
        webSocketManager.webSocketMessageReceiver = self
        webSocketManager.meetUp = meetUp!
        webSocketManager.phoneInfo = SessionManager.sharedsessionManager.userSession!.userInfo!.phoneInfo
        webSocketManager.authToken = SessionManager.sharedsessionManager.userSession!.authToken!
        webSocketManager.connectToHub()
        return webSocketManager
    }
   
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        mapViewManager.removeObserverForMap(self.myMap)
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func viewWillAppear(animated: Bool) {
        mapViewManager.addObserverForMap(self.myMap)
        self.myMap!.padding = UIEdgeInsetsMake(64, 0, 64, 0);
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        self.deregisterInternetStatus()
    }
    
    
}

