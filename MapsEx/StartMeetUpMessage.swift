//
//  StartMeetUpMessage.swift
//  MapsEx
//
//  Created by Pradeep on 03/12/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
class StartMeetUpMessage{
    var admin: UserInfo
    var meetUpLocation: Location
    var startUpTime: NSDate
    var invitees: [UserInfo]
    
    init(){
        admin = UserInfo()
        meetUpLocation = Location()
        startUpTime = NSDate()
        invitees = [UserInfo]()
    }
    
    func toDict() -> Dictionary<String,AnyObject> {
        var dictionary : Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dictionary["admin"] = admin.toDict()
        dictionary["meetUpLocation"] = meetUpLocation.toDict()
        dictionary["startUpTime"] = dateToString()
        dictionary["invitees"] = invitees.map{$0.toDict()}
        return dictionary
    }
    
    func dateToString() -> String {
        var nsDateFormatter = NSDateFormatter()
        nsDateFormatter.dateFormat = "MMM d, yyyy h:mm:ss a"
        return nsDateFormatter.stringFromDate(startUpTime)
    }
    
}