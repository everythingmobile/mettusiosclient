//
//  MeetUpDetailPresenter.swift
//  MapsEx
//
//  Created by Pradeep on 31/01/15.
//  Copyright (c) 2015 pradeep. All rights reserved.
//

import Foundation
class MeetUpDetailPresenter {
    init(){
        
    }
    var sessionManager: SessionManager?
    
    func getFormattedDate(meetUp: MeetUpEntity?) -> String{
        if(meetUp != nil){
            return getformatDateFromNSDate(meetUp!.startUpTime,zone: "GMT")
        }
        return ""
    }
    
    func getformatDateFromNSDate(date: NSDate, zone : String = "") -> String{
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMM d, yyyy h:mm:ss a" //"yyyy-MM-dd 'at' h:mm a" // superset of OP's format
        if(zone != ""){
            dateFormatter.timeZone = NSTimeZone(name: zone)
        }
        let str = dateFormatter.stringFromDate(date)
        return str
    }
    
    func getformatDateFromNSDate(date: NSDate) -> String{
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMM d, yyyy h:mm:ss a" //"yyyy-MM-dd 'at' h:mm a" // superset of OP's format
        dateFormatter.timeZone = NSTimeZone(name: "GMT")
        let str = dateFormatter.stringFromDate(date)
        return str
    }
    
    
    func getImage(meetUp: MeetUpEntity?, completionHandler: (UIImage) -> ()) {
        if(meetUp != nil){
            var lat = meetUp!.meetUpLocation.destinationLatitude
            var long = meetUp!.meetUpLocation.destinationLongitude
            var req = request(.GET,"http://maps.googleapis.com/maps/api/staticmap?center=\(lat),\(long)&zoom=14&markers=color:red%7C\(lat)%2C\(long)&size=400x169&key=AIzaSyDYBCUsh_MxgUzXXA2bCXtFKbhTVTF0HjQ")
            req.response { (request, response, data, error) -> Void in
                if let imgData = data as? NSData{
                    var image = UIImage( data : imgData)
                    //println("image recvd")
                    NSOperationQueue.mainQueue().addOperationWithBlock{
                        () -> Void in
                        completionHandler(image!)
                    }
                }
            }
        }
    }
    
    func getCurrentUser()-> PhoneInfo? {
        return sessionManager?.userSession?.userInfo?.phoneInfo
    }
    
    func isCurrentUserAdmin(phoneInfo: PhoneInfo) -> Bool{
        if let userPhoneInfo = getCurrentUser(){
            return userPhoneInfo == phoneInfo
        }
        return false
    }
    
    func isCurrentUserInMeetUp(meetUp: MeetUpEntity) -> Bool {
        if let userPhoneInfo = getCurrentUser() {
            for participant in meetUp.participants {
                if(userPhoneInfo == participant.phoneInfo) {
                    return true
                }
            }
        }
        return false
    }
    
}
