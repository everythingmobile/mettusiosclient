//
//  Router.swift
//  MapsEx
//
//  Created by Pradeep on 13/11/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
import UIKit
import AddressBookUI

class Router: NSObject{
    var msDynamicsViewController: MSDynamicsDrawerViewController
    var  boards =  ["MeetUp Feed", "MENU_BOARD", "Location Search", "MeetUp", "Contacts", "My MeetUps", "CONTACTS_PICKER"]
    var navigationViewControllers: [String: UINavigationController] =  [String: UINavigationController]()
    var viewControllers: [String: UIViewController] =  [String: UIViewController]()
    var storyboard:  UIStoryboard
    
    init(storyboard: UIStoryboard,msDynamicsViewController: MSDynamicsDrawerViewController){
        self.storyboard = storyboard
        self.msDynamicsViewController = msDynamicsViewController
    }
    
    func setPaneViewController(identifier: String,animated: Bool,completion: (()->())?){
        var paneController =  getNavigationController(identifier)
        msDynamicsViewController.setPaneViewController(paneController, animated: animated, completion: completion)
    }
    
    func setPaneViewController(identifier: String){
        var paneController =  getNavigationController(identifier)
        msDynamicsViewController.paneViewController = paneController
    }
    
    func setPaneViewController(#controller: UIViewController,animated: Bool,completion: (()->())?){
        configureVC(controller)
        assignRouter(controller)
        /*msDynamicsViewController.setPaneViewController(UINavigationController(rootViewController: controller), animated: animated, completion: completion)*/
        var nc = UINavigationController(rootViewController: controller)
        nc.navigationBar.tintColor = UIColor.whiteColor()
        nc.navigationBar.barTintColor = UIColor(red: 54.0 / 255.0, green: 215.0 / 255.0, blue: 183.0 / 255.0, alpha: 1)
            //UIColor(red: 29.0 / 255.0, green: 204.0 / 255.0, blue: 198.0 / 255.0, alpha: 1)
        nc.navigationBar.translucent = false
        nc.navigationBar.titleTextAttributes = [NSFontAttributeName : UIFont(name: "Optima-Bold", size: 20)!, NSForegroundColorAttributeName : UIColor.whiteColor()]
        msDynamicsViewController.setPaneViewController(nc, animated: animated, completion: completion)
    }
    
    
    func getNavigationController(identifier: String)-> UINavigationController{
        if(navigationViewControllers[identifier] == nil){
            let vc = getController(identifier)
            configureVC(vc)
            assignRouter(vc)
            /*navigationViewControllers[identifier] = UINavigationController(rootViewController: vc)
            navigationViewControllers[identifier] = nc*/
            var nc = UINavigationController(rootViewController: vc)
            nc.navigationBar.barTintColor =  UIColor(red: 54.0 / 255.0, green: 215.0 / 255.0, blue: 183.0 / 255.0, alpha: 1)
            //UIColor(red: 29.0 / 255.0, green: 204.0 / 255.0, blue: 198.0 / 255.0, alpha: 1)
            nc.navigationBar.tintColor = UIColor.whiteColor()
            nc.navigationBar.translucent = false
            nc.navigationBar.titleTextAttributes = [NSFontAttributeName : UIFont(name: "Optima-Bold", size: 20)!, NSForegroundColorAttributeName : UIColor.whiteColor()]
            navigationViewControllers[identifier] = nc
        }
        return  navigationViewControllers[identifier]!
    }
    
    func getController(identifier: String,cache: Bool = true) -> UIViewController{
        if(viewControllers[identifier] == nil && cache  == true){
            let vc = self.storyboard.instantiateViewControllerWithIdentifier(identifier) as! UIViewController
            viewControllers[identifier] = vc
        }
        else if(cache == false){
            return self.storyboard.instantiateViewControllerWithIdentifier(identifier) as! UIViewController
        }
        return viewControllers[identifier]!
        
    }
    
    func assignRouter(vc: UIViewController){
        if let rp = vc as? RouterProtocol{
            rp.router = self
        }
    }
    
    func segueToAddContactsToMeetUp(meetUpLocation: Location){
        let dc  = self.storyboard.instantiateViewControllerWithIdentifier("CONTACTS_PICKER") as! ContactsPickerViewController
        dc.title = "select contacts"
        dc.meetUpLocation = meetUpLocation
        self.setPaneViewController(controller: dc, animated: false, completion: nil)
    }
    
    func segueToChooseMeetUpDate(startMeetUpMessage: StartMeetUpMessage){
        
        let dc  = self.storyboard.instantiateViewControllerWithIdentifier("MEETUP_DATEPICKER") as! MeetUpStartTimeSelectionViewController
        dc.title = "select meet up date"
        dc.startMeetUpMessage =  startMeetUpMessage
        self.setPaneViewController(controller: dc, animated: false, completion: nil)
        
    }
    
    func segueToMeetUpParticpants(#sender: UIViewController,meetUp: MeetUpEntity?){
        if(meetUp != nil){
            let dc = self.storyboard.instantiateViewControllerWithIdentifier("PARTICIPANTS") as! MeetUpParticipantsTableViewController
            dc.title = "Participants"
            dc.meetUp = meetUp!
            dc.router = self
            sender.navigationController?.pushViewController(dc, animated: true)
        }
    }
    
    func segueToCreateMeetUp(){
        let vc = self.storyboard.instantiateViewControllerWithIdentifier("Location Search") as! LocationSearchController
        vc.title = "create meetup"
        self.setPaneViewController(controller: vc, animated: false, completion: nil)
    }
    
    func segueToMeetUpDetail(#sender: UIViewController,meetUp: MeetUpEntity, isMyMeetUp: Bool = false){
        let vc = self.storyboard.instantiateViewControllerWithIdentifier("MEETUP_DETAIL") as! MeetUpDetailViewController
        vc.shouldShowInviteButton = isMyMeetUp
        vc.meetUp = meetUp
        vc.router = self
        sender.navigationController?.pushViewController(vc, animated: true)
    }
    
    func segueToAddContactsToInvite(#sender: UIViewController,meetUpEntityChanges: MeetUpEntityChanges){
        let dc  = self.storyboard.instantiateViewControllerWithIdentifier("CONTACTS_PICKER") as! ContactsPickerViewController
        dc.title = "select contacts"
        dc.meetUpEntityChanges = meetUpEntityChanges
        dc.nextSegue = NextSegueFromContacts.InviteContacts
        sender.navigationController?.pushViewController(dc, animated: true)
    }
    
    func segueToUserComments(#sender: UIViewController, meetUp: MeetUpEntity) {
        let dc = self.storyboard.instantiateViewControllerWithIdentifier("USER_COMMENT") as! UserCommentViewController
        dc.title = "Comments"
        dc.meetUp = meetUp
        sender.navigationController?.pushViewController(dc, animated: true)
    }
    
    func segueToMeetUpMap(meetUp: MeetUpEntity){
        
        let vc = self.storyboard.instantiateViewControllerWithIdentifier("MeetUp") as! mapViewController
        vc.title = "meetup"
        vc.meetUp = meetUp
        self.setPaneViewController(controller: vc, animated: false, completion: nil)
    }
    
    func segueToContactPicker(#sender: UIViewController) {
        let cp = ABPeoplePickerNavigationController()
        sender.navigationController?.pushViewController(cp, animated: true)
    }
    
    func logOut(#sender: UIViewController){
        SessionManager.sharedsessionManager.logOut()
        let vc = self.storyboard.instantiateViewControllerWithIdentifier("LOGIN_BOARD") as! UIViewController
        msDynamicsViewController.setPaneViewController(vc, animated: true, completion: {() -> Void  in
            self.msDynamicsViewController.setDrawerViewController(nil, forDirection: MSDynamicsDrawerDirection.Left)
            })
       
        //msDynamicsViewController.presentViewController(vc, animated: true, completion: nil)
    }
    
    func setDrawerViewContoller(#identifier: String,forDirection direction: MSDynamicsDrawerDirection){
        msDynamicsViewController.setDrawerViewController(getController(identifier), forDirection: MSDynamicsDrawerDirection.Left)

    }
    
    func setDrawerViewContoller(#controller: UIViewController,forDirection direction: MSDynamicsDrawerDirection){
        msDynamicsViewController.setDrawerViewController(controller, forDirection: MSDynamicsDrawerDirection.Left)
    }
    
    func configureVC(viewController: UIViewController){
        let view = viewController.view
        if(viewController.searchDisplayController != nil){
         viewController.searchDisplayController!.displaysSearchBarInNavigationBar = true
        }
        var paneRevealLeftBarButtonItem = UIBarButtonItem(image: UIImage(named: "ham"), style: UIBarButtonItemStyle.Bordered, target: self, action: "dynamicsDrawerRevealLeftBarButtonItemTapped")
        viewController.navigationItem.leftBarButtonItem = paneRevealLeftBarButtonItem;
    }
    
    func dynamicsDrawerRevealLeftBarButtonItemTapped()
    {
        self.msDynamicsViewController.setPaneState( MSDynamicsDrawerPaneState.Open, inDirection: MSDynamicsDrawerDirection.Left, animated: true, allowUserInterruption: true, completion: nil)
    }
    
}