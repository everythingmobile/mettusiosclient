//
//  CreateMeetUpsPresenter.swift
//  MapsEx
//
//  Created by Pradeep on 06/12/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
class CreateMeetUpsPresenter{
    var sessionManager: SessionManager?
    var meetUpManager: MeetUpManager?
    init(){
        
    }
    
    func createMeetUp(var startMeetUpMessage: StartMeetUpMessage){
        var userInfo = sessionManager?.userSession?.userInfo
        if (userInfo != nil){
           startMeetUpMessage.admin = userInfo!
            meetUpManager?.createMeetUp(startMeetUpMessage)
        }
    }
    
    func inviteContacts(inviteMessage: InviteMessage){
        var userInfo = sessionManager?.userSession?.userInfo
        if (userInfo != nil){
            inviteMessage.setInviter(userInfo!)
            meetUpManager?.inviteContacts(inviteMessage)
        }
    }
    
    
    func updateMeetup(meetUpEntityChanges: MeetUpEntityChanges){
        meetUpManager?.updateMeetUp(meetUpEntityChanges)
    }
    
}