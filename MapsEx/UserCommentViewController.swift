//
//  UserCommentViewController.swift
//  MapsEx
//
//  Created by Pradeep Borado on 18/04/15.
//  Copyright (c) 2015 pradeep. All rights reserved.
//

import UIKit

class UserCommentViewController: UIViewController {
    
    var userCommentPresenter: UserCommentPresenter?
    var meetUpDetailPresenter: MeetUpDetailPresenter?
    var meetUp : MeetUpEntity?
    var commentsTableViewDSnDG = UserCommentDataSource()

    override func viewDidLoad() {
        super.viewDidLoad()
        userCommentPresenter = getUserCommentPresenter()
        meetUpDetailPresenter = getMeetUpDetailPresenter()
        configureCommentTextBoxAndButton()
        self.userCommentTableView.dataSource = commentsTableViewDSnDG
        self.userCommentTableView.delegate = commentsTableViewDSnDG
        getMeetUpComments()
        registerForRefreshNotification()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configureCommentTextBoxAndButton() {
        if(meetUp == nil || meetUpDetailPresenter?.isCurrentUserInMeetUp(meetUp!) != true) {
            self.commentsTextField.hidden = true
            self.sendCommentButton.hidden = true
        }
    }
    
    func getMeetUpDetailPresenter() -> MeetUpDetailPresenter{
        var meetUpDetailPresenter = MeetUpDetailPresenter()
        meetUpDetailPresenter.sessionManager = SessionManager.sharedsessionManager
        return meetUpDetailPresenter
    }

    @IBOutlet weak var userCommentTableView: UITableView!
    @IBOutlet weak var commentsTextField: UITextField!
    
    @IBOutlet weak var sendCommentButton: UIButton!
    @IBAction func sendComment(sender: AnyObject) {
        if(commentsTextField.text != "" && meetUp != nil) {
            userCommentPresenter?.addUserComment(meetUp!.meetUpId, comment: commentsTextField.text)
        }
    }
    
    @IBAction func sendButtonPressed(sender: AnyObject) {
        if(commentsTextField.text != "" && meetUp != nil) {
            userCommentPresenter?.addUserComment(meetUp!.meetUpId, comment: commentsTextField.text)
            commentsTextField.text = ""
        }
    }
    
    func getUserCommentPresenter() -> UserCommentPresenter {
        let ucp = UserCommentPresenter()
        ucp.sessionManager = SessionManager.sharedsessionManager
        ucp.userCommentManager = UserCommentManager()
        return ucp
    }
    
    func getMeetUpComments() {
        if(meetUp != nil) {
            userCommentPresenter?.getMeetUpComments(meetUpId: meetUp!.meetUpId, completionHandler: { (userAndUserComments) -> () in
                NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                     self.commentsTableViewDSnDG.userAndUserComments = userAndUserComments
                    self.userCommentTableView.reloadData()
                })
            })
        }
    }
    
    func refreshCommentsTableView(notification: NSNotification) {
        getMeetUpComments()
    }
    
    
    func registerForRefreshNotification(){
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "refreshCommentsTableView:", name: "refreshComments", object: nil)
    }

    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "refreshComments", object: nil)
    }
    
    /*
    @IBAction func sendComment(sender: AnyObject) {
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
