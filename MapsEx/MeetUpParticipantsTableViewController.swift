//
//  MeetUpParticipantsTableViewController.swift
//  MapsEx
//
//  Created by Pradeep Borado on 06/03/15.
//  Copyright (c) 2015 pradeep. All rights reserved.
//

import UIKit

class MeetUpParticipantsTableViewController: UITableViewController, SWTableViewCellDelegate {

    var meetUp: MeetUpEntity?
    var participants: [UserInfo] = [UserInfo]()
    var updateMeetUpPresenter: CreateMeetUpsPresenter?
    var removedParticipants: [UserInfo] = [UserInfo]()
    var router: Router?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(meetUp != nil){
            participants = getFriendsList(meetUp!)
        }
        updateMeetUpPresenter = getUpdateMeetUpPresenter()
        configureInviteButton()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    func getFriendsList(currentMeetUp: MeetUpEntity) -> [UserInfo] {
        var friendsList = [UserInfo]()
        friendsList = currentMeetUp.friends.map { (index: Int) -> UserInfo in
            return currentMeetUp.participants[index]
        }
        return friendsList
    }
    
    func getUpdateMeetUpPresenter() -> CreateMeetUpsPresenter {
        let createMeetUpsPresenter = CreateMeetUpsPresenter()
        createMeetUpsPresenter.sessionManager = SessionManager.sharedsessionManager
        createMeetUpsPresenter.meetUpManager = MeetUpManager()
        return createMeetUpsPresenter
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return participants.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:SWTableViewCell = self.tableView.dequeueReusableCellWithIdentifier("cell") as! SWTableViewCell
        let userInfo = participants[indexPath.row]
        cell.textLabel!.text = userInfo.userName
        cell.detailTextLabel!.text = userInfo.phoneInfo.countryCode + " " + userInfo.phoneInfo.phoneNumber
        cell.leftUtilityButtons = leftButtons() as [AnyObject]
        cell.delegate = self
        return cell
    }

    func leftButtons() -> NSArray{
        var lb = NSMutableArray()
        lb.sw_addUtilityButtonWithColor(UIColor(red: 1.0, green: 0.231, blue: 0.188, alpha: 1), title: "Delete")
        return lb
    }
    
    func swipeableTableViewCell(cell: SWTableViewCell!, didTriggerLeftUtilityButtonWithIndex index: Int) {
        switch(index){
        case 0:
            let indexPath = self.tableView.indexPathForCell(cell)
            self.removedParticipants.append(self.participants[indexPath!.row])
            self.participants.removeAtIndex(indexPath!.row)
            if(self.removedParticipants.count == 1){
                self.configureUpdateMeetUpButton()
            }
            tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: UITableViewRowAnimation.Automatic)
            println("Delet was pressed")
        default:
            println("Nothing")
        }
    }
    
    func updateMeetUp(){
        let meetUpEntityChanges = MeetUpEntityChanges()
        meetUpEntityChanges.meetUpId = meetUp!.meetUpId
        meetUpEntityChanges.removedUsers = self.removedParticipants
        updateMeetUpPresenter?.updateMeetup(meetUpEntityChanges)
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    func swipeableTableViewCell(cell: SWTableViewCell!, canSwipeToState state: SWCellState) -> Bool {
        if(state == SWCellState.CellStateLeft){
            return true
        }
        return false
    }
    
    func configureUpdateMeetUpButton(){
        var button = UIBarButtonItem(title: "Save", style: UIBarButtonItemStyle.Done, target: self, action: "updateMeetUp")
        self.navigationItem.rightBarButtonItem = button
    }
    
    func configureInviteButton(){
        if(meetUp != nil && getMeetUpDetailPresenter().isCurrentUserAdmin(meetUp!.adminPhoneInfo) == true ){
            //var inviteButton = UIBarButtonItem(title: "Invite", style: UIBarButtonItemStyle.Plain, target: self, action: "inviteContacts")
            var inviteButton = UIBarButtonItem(image: UIImage(named: "group"), style: UIBarButtonItemStyle.Plain, target: self, action: "inviteContacts")
            self.navigationItem.rightBarButtonItem = inviteButton
        }
    }
    
    func getMeetUpDetailPresenter() -> MeetUpDetailPresenter{
        var meetUpDetailPresenter = MeetUpDetailPresenter()
        meetUpDetailPresenter.sessionManager = SessionManager.sharedsessionManager
        return meetUpDetailPresenter
    }
    
    func inviteContacts(){
        if(meetUp != nil){
            var meetUpEntityChanges = MeetUpEntityChanges()
            meetUpEntityChanges.meetUpId =  meetUp!.meetUpId
            router?.segueToAddContactsToInvite(sender: self,meetUpEntityChanges: meetUpEntityChanges)
        }
    }


    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
