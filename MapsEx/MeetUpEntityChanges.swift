//
//  MeetUpEntityChanges.swift
//  MapsEx
//
//  Created by Pradeep on 28/02/15.
//  Copyright (c) 2015 pradeep. All rights reserved.
//

import Foundation
class MeetUpEntityChanges{
    var meetUpId: String
    var addedUsers: [UserInfo]?
    var removedUsers: [UserInfo]?
    var startUpTime: NSDate?
    
    init(){
        meetUpId = ""
        addedUsers = nil
        removedUsers = nil
        startUpTime = nil
    }
    
    func toDict () -> Dictionary<String,AnyObject> {
        var dict =  Dictionary<String,AnyObject>()
        dict["meetUpId"] = self.meetUpId
        if addedUsers != nil {
            dict["addedUsers"] = addedUsers!.map{$0.toDict()}
        }
        if removedUsers != nil{
            dict["removedUsers"] = removedUsers!.map{$0.toDict()}
        }
        if startUpTime != nil{
            dict["startUpTime"] = dateToString()
        }
        
        return dict
    }
    
    func dateToString() -> String {
        var nsDateFormatter = NSDateFormatter()
        nsDateFormatter.dateFormat = "MMM d, yyyy h:mm:ss a"
        return nsDateFormatter.stringFromDate(startUpTime!)
    }
}
