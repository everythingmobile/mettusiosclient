//
//  EnumHelpers.swift
//  MapsEx
//
//  Created by Pradeep on 13/12/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
enum NextSegueFromContacts{
    case InviteContacts
    case CreateMeetUp
}