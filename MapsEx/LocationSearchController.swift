//
//  LocationSearchController.swift
//  MapsEx
//
//  Created by Pradeep on 09/11/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
import UIKit
class LocationSearchController: UIViewController,UISearchDisplayDelegate,UITableViewDelegate,UITableViewDataSource,RouterProtocol,GMSMapViewDelegate{
    var locationSearchPresenter: LocationSearchPresenter?
    var places = Array<AutoCompleteResult>()
    var nextButton: UIButton?
    var router: Router?
    var meetUpLocation: Location = Location()
    var markers = [PlaceMarker]()
    var selectedMarker: PlaceMarker? = nil
    @IBOutlet weak var mapView: GMSMapView!

    @IBOutlet weak var searchBar: UISearchBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchDisplayController!.displaysSearchBarInNavigationBar = true
        self.locationSearchPresenter = LocationSearchPresenter()
        self.locationSearchPresenter!.locationSearchManager = LocationSearchManager()
        self.mapView.myLocationEnabled=true
        self.mapView.settings.myLocationButton=true
        self.mapView.settings.compassButton=true
        self.mapView.delegate = self
        var location=mapView.myLocation
        if (location != nil){
            println("cur loc")
            mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 17, bearing: 0, viewingAngle: 0)
        }
            }
    
    func searchDisplayController(controller: UISearchDisplayController, shouldReloadTableForSearchString searchString: String) -> Bool {
        self.locationSearchPresenter?.searchPlace(searchString, completionHandler: { [unowned self](results) -> () in
            NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                self.places = results
                self.searchDisplayController?.searchResultsTableView.reloadData()
            })
        })
        return false
    }
    
    /*func mapView(mapView: GMSMapView!, idleAtCameraPosition position: GMSCameraPosition!) {
        self.fetchNearByPlaces(self.mapView.camera.target)
    }*/
    
    /*func didTapMyLocationButtonForMapView(mapView: GMSMapView!) -> Bool {
        var location = mapView.myLocation.coordinate
        self.fetchNearByPlaces(location)
        return true
    }*/
    
    func configureNextButton(){
        if(self.nextButton == nil){
            let button   = UIButton.buttonWithType(UIButtonType.System) as! UIButton
            button.backgroundColor = UIColor.whiteColor()
            button.frame = CGRectMake(self.view.frame.size.width-130, self.view.frame.size.height-100, 100, 50)
            button.setTitle("Next", forState: UIControlState.Normal)
            button.addTarget(self, action: "segueToChooseContacts", forControlEvents: UIControlEvents.TouchUpInside)
            self.view.insertSubview(button, aboveSubview: self.mapView)
            self.nextButton = button
        }
    }
    
    func segueToChooseContacts(){
        router?.segueToAddContactsToMeetUp(self.meetUpLocation)
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.searchDisplayController?.active = false
        self.getLocationFromAutoCompleteResult(self.places[indexPath.row],clearMap: true)
    }
    
    func getLocationFromAutoCompleteResult(autoCompleteResult: AutoCompleteResult, clearMap: Bool =  false){
        locationSearchPresenter?.getLocation(fromAutoCompleteResult: autoCompleteResult, onRetreiveingLocation: { (geoCodedObject) -> () in
            NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                self.setMeetUpLocation(geoCodedObject)
                let camera = GMSCameraPosition(target: geoCodedObject.location, zoom: 15, bearing: 0, viewingAngle: 0 )
                if(clearMap == true){
                    self.mapView.clear()
                    self.selectedMarker = nil
                    var marker  = PlaceMarker(coordinate: geoCodedObject.location,place_id: autoCompleteResult.place_id,address: geoCodedObject.formatted_address)
                    marker.title = geoCodedObject.name
                    marker.icon = GMSMarker.markerImageWithColor(UIColor.redColor())
                    marker.map = self.mapView
                    self.selectedMarker = marker
                    self.mapView.animateToCameraPosition(camera)
                    self.fetchNearByPlaces(geoCodedObject.location)
                }
                self.configureNextButton()
            })
        })
    }
    
    func mapView(mapView: GMSMapView!, didTapMarker marker: GMSMarker!) -> Bool {
        if let placeMarker = marker  as? PlaceMarker{
            if(placeMarker.place_id != self.selectedMarker!.place_id) {
                let geocodedObject = GeocodedObject()
                geocodedObject.name = placeMarker.title
                geocodedObject.formatted_address = placeMarker.address
                geocodedObject.location = placeMarker.position
                self.setMeetUpLocation(geocodedObject)
                placeMarker.icon = GMSMarker.markerImageWithColor(UIColor.redColor())
                if(self.selectedMarker != nil){
                    self.selectedMarker!.icon = GMSMarker.markerImageWithColor(UIColor.blackColor())
                }
                self.selectedMarker = placeMarker
            }
        }
        return false
    }
    
    func setMeetUpLocation(geoCodedObject: GeocodedObject){
        self.meetUpLocation.locationDescription = geoCodedObject.name
        self.meetUpLocation.locationAddress = geoCodedObject.formatted_address
        self.meetUpLocation.destinationLatitude = "\(geoCodedObject.location.latitude)"
        self.meetUpLocation.destinationLongitude = "\(geoCodedObject.location.longitude)"
    }
    
    func numberOfSectionsInTableView(tableView:UITableView)->Int{
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell? = tableView.dequeueReusableCellWithIdentifier("cell") as? UITableViewCell
        if(cell == nil){
            cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "Cell")

        }
        cell!.selectionStyle = UITableViewCellSelectionStyle.None
        cell!.textLabel!.text = String(self.places[indexPath.row].description)
        return cell!
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.places.count
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    
    var mapRadius: Double {
        get {
            let region = mapView.projection.visibleRegion()
            let verticalDistance = GMSGeometryDistance(region.farLeft, region.nearLeft)
            let horizontalDistance = GMSGeometryDistance(region.farLeft, region.farRight)
            return max(horizontalDistance, verticalDistance)*0.5
        }
    }
    
    func fetchNearByPlaces(coordinate: CLLocationCoordinate2D){
        let dataProvider = GoogleDataProvider()
        dataProvider.fetchPlacesNearCoordinate(coordinate,radius: mapRadius) { places in
            self.markers = [PlaceMarker]()
            for place: GooglePlace in places {
                NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                    if (self.selectedMarker == nil || place.place_id != self.selectedMarker!.place_id) {
                        let marker = PlaceMarker(coordinate: place.coordinate,place_id: place.place_id, address: place.address)
                        marker.title = place.name
                        marker.map = self.mapView
                        self.markers.append(marker)
                    }
                })
            }
        }
    }
    
}
