//
//  WebSocketMessageReceiver.swift
//  MapsEx
//
//  Created by Pradeep on 19/10/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
protocol WebSocketMessageReceiver{
    func messageReceivedHandler(dict : Dictionary<String,String>)
}