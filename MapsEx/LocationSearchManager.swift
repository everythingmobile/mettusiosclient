//
//  LocationSearchManager.swift
//  MapsEx
//
//  Created by Pradeep on 09/11/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
class LocationSearchManager{
    let API_KEY = "AIzaSyBxlQ42gy8MuhbkgCMzm0IpMXMS64WJ_fA"
    init(){
        
    }
    func searchPlace(place: String,completionHandler: ([AutoCompleteResult])->()){
        var placeToSearch = place.stringByReplacingOccurrencesOfString(" ", withString: "+", options: NSStringCompareOptions.LiteralSearch, range: nil)
        let url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input="+placeToSearch+"&key="+API_KEY
        request(.GET, url, parameters: nil).responseJSON { (req, res, data, err) -> Void in
            if(err == nil){
                var dict = data as! NSDictionary
                var status = dict["status"] as! String
                var places = [AutoCompleteResult]()
                if(status == "OK"){
                    var predictions  = dict["predictions"] as! NSArray
                    for prediction in predictions{
                        var place = prediction as! NSDictionary
                        var description = place["description"] as! String
                        var place_id = place["place_id"] as! String
                        places.append(AutoCompleteResult(description: description, place_id: place_id))
                    }
                    completionHandler(places)
                }
                else{
                    println("cannot retrieve auto complete results")
                }
                
            }
            else{
                println("cannot search location "+place)
            }
        }
    }
    
    func geocodeLocation(place: String,onGeocodingComplete: (GeocodedObject)->()){
        var placeToSearch = place.stringByReplacingOccurrencesOfString(" ", withString: "+", options: NSStringCompareOptions.LiteralSearch, range: nil)
        let url = "https://maps.googleapis.com/maps/api/geocode/json?address="+placeToSearch+"&key="+API_KEY
        request(.GET, url).responseJSON { (req, res, data, err) -> Void in
            if(err == nil){
                let json = data as! NSDictionary
                var status = json["status"] as! String
                if(status == "OK"){
                    var results = json["results"] as! NSArray
                    var result = results[0] as! NSDictionary
                    var firstResult = result.mutableCopy() as! NSMutableDictionary
                    firstResult["name"] = place
                    var geocodedObject = GeocodedObject(fromDictionary: firstResult)
                    onGeocodingComplete(geocodedObject)
                }
            }
        }

    }
    
    func getLocation(fromAutoCompleteResult autoCompleteResult: AutoCompleteResult,onRetreiveingLocation: (GeocodedObject) -> ()){
        let url = "https://maps.googleapis.com/maps/api/place/details/json?placeid="+autoCompleteResult.place_id+"&key="+API_KEY
        request(.GET,url).responseJSON { (request, response, data, error) -> Void in
            if(error == nil){
                let json = data as! NSDictionary
                var status = json["status"] as! String
                if(status == "OK") {
                    var result = json["result"] as! NSDictionary
                    var firstResult = result.mutableCopy() as! NSMutableDictionary
                    //firstResult["name"] = autoCompleteResult.description
                    var geocodedObject = GeocodedObject(fromDictionary: firstResult)
                    onRetreiveingLocation(geocodedObject)
                }
            }
        }
    }
    
    
    
}