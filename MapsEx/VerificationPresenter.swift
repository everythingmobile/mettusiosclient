//
//  VerificationPresenter.swift
//  MapsEx
//
//  Created by Pradeep on 08/11/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
class VerificationPresenter{
    init(){
        
    }
    var verificationManager: VerificationManger?
    var sessionManager: SessionManager?
    func verifyUser(verification: Verification,flag: Int,verifyUserCompletionHandler:(isVerified: Bool)->()){
        if(flag == 1){
            var userInfo = UserInfo(userName: "", phoneInfo:  verification.phoneInfo! , emailId: "", registered: true)
            sessionManager?.setUserInfo(userInfo)
        }        
        verificationManager?.verifyUser(verification,completionHandler: {[unowned self](tokens,error)->() in
            if(error != nil){
                println("cant verify")
                verifyUserCompletionHandler(isVerified: false)
            }
            else{
                
                if let authToken = tokens?.authToken {
                    println(authToken)
                    if(authToken != ""){
                        self.sessionManager?.setAuthToken(authToken)
                        var userInfo = UserInfo()
                        userInfo.userName = tokens!.userName
                        userInfo.phoneInfo = tokens!.phoneInfo
                        self.sessionManager?.setUserInfo(userInfo)
                        verifyUserCompletionHandler(isVerified: true)
                    }
                    else{
                        verifyUserCompletionHandler(isVerified: false)
                    }
                }
                else{
                    verifyUserCompletionHandler(isVerified: false)
                }
            }
        })
    }
    
    func getUserPhoneInfo()-> PhoneInfo{
        return sessionManager!.userSession!.userInfo!.phoneInfo
    }
    
    
}