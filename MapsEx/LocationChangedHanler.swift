//
//  LocationChangedHanler.swift
//  MapsEx
//
//  Created by Pradeep on 20/10/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
protocol LocationChangedHanler{
    func handleLocationChange(message : Dictionary<String,String>)
}
