
import Foundation

class PlaceMarker: GMSMarker {
  // 1
  var place_id: String
    var address: String
  
  // 2
    init(coordinate: CLLocationCoordinate2D,place_id: String, address: String) {
    self.place_id = place_id
    self.address = address
    super.init()
    
    position = coordinate
    icon = GMSMarker.markerImageWithColor(UIColor.blackColor())
    groundAnchor = CGPoint(x: 0.5, y: 1)
    appearAnimation = kGMSMarkerAnimationPop
  }
    

}