//
//  FeedDataSource.swift
//  MapsEx
//
//  Created by Pradeep on 08/11/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
import UIKit
class UserCommentDataSource: NSObject,UITableViewDataSource, UITableViewDelegate{
    var userAndUserComments = [UserAndUserComment]()
    
    func numberOfSectionsInTableView(tableView:UITableView)->Int{
        return 1
    }
    
    func tableView(tableView:UITableView,numberOfRowsInSection section:Int)->Int{
        return userAndUserComments.count
    }
    func tableView(tableView:UITableView,cellForRowAtIndexPath indexPath:NSIndexPath)->UITableViewCell{
        var cell:UserCommentViewCell? = tableView.dequeueReusableCellWithIdentifier("cell") as? UserCommentViewCell
        if(cell == nil){
            cell = UserCommentViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        }
        
        cell!.commentLabel!.text = userAndUserComments[indexPath.row].userComment.comment
        cell!.userNameLabel!.text = userAndUserComments[indexPath.row].userInfo.userName
        cell!.commentTimeLabel!.text = getFormattedDate(userAndUserComments[indexPath.row].userComment.commentTime)
        
        return cell!
    }
    
    func getFormattedDate(date: NSDate) -> String{
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMM d, yyyy h:mm:ss a" //"yyyy-MM-dd 'at' h:mm a" // superset of OP's format
        dateFormatter.timeZone = NSTimeZone(name: "GMT")
        let str = dateFormatter.stringFromDate(date)
        return str
        
    }
    
}