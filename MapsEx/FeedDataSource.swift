//
//  FeedDataSource.swift
//  MapsEx
//
//  Created by Pradeep on 08/11/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
import UIKit
class FeedDataSource: NSObject,UITableViewDataSource {
    var meetUpFeed = [MeetUpEntity]()
    
    func numberOfSectionsInTableView(tableView:UITableView)->Int {
        return 1
    }
    
    func tableView(tableView:UITableView,numberOfRowsInSection section:Int)->Int {
        return meetUpFeed.count
    }
    
    func tableView(tableView:UITableView,cellForRowAtIndexPath indexPath:NSIndexPath)->UITableViewCell {
        var cell : FeedViewCell? = tableView.dequeueReusableCellWithIdentifier("Cell") as? FeedViewCell
        if cell == nil {
            cell = FeedViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "Cell")
        }
        cell!.subjectLabel.numberOfLines = 0
        cell!.subjectLabel.text = self.makeFeedDescription(meetUpFeed[indexPath.row])
        cell!.mainView.layer.cornerRadius = 3
        cell!.mainView.layer.masksToBounds = true
        cell!.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        cell!.tag = indexPath.row
        var lat = meetUpFeed[indexPath.row].meetUpLocation.destinationLatitude
        var long = meetUpFeed[indexPath.row].meetUpLocation.destinationLongitude
        var req = request(.GET,"http://maps.googleapis.com/maps/api/staticmap?center=\(lat),\(long)&zoom=14&markers=color:red%7C\(lat)%2C\(long)&size=265x150&key=AIzaSyDYBCUsh_MxgUzXXA2bCXtFKbhTVTF0HjQ")
        req.response { (request, response, data, error) -> Void in
            if let imgData = data as? NSData{
                var image = UIImage( data : imgData)
                //println("image recvd")
                NSOperationQueue.mainQueue().addOperationWithBlock{
                    () -> Void in
                    if cell!.tag == indexPath.row {
                        cell!.mapImage.image = image
                        cell!.setNeedsLayout()
                        
                    }
                    
                }
            }
        }
        return cell!
    }
    
    
    func makeFeedDescription(meetUpEntity : MeetUpEntity)-> String {
        var feedText = String()
        if(meetUpEntity.friends.count == 1){
            feedText = feedText + meetUpEntity.participants[meetUpEntity.friends[0]].userName + " is going to meetup."
        }
        else if(meetUpEntity.friends.count == 2){
            feedText = feedText + " \(meetUpEntity.participants[meetUpEntity.friends[0]].userName ), \(meetUpEntity.participants[meetUpEntity.friends[1]].userName )"
                + " and \(meetUpEntity.participants.count-2) others are meeting up."
        }
        else if (meetUpEntity.friends.count  > 2) {
            feedText = feedText + " \(meetUpEntity.participants[meetUpEntity.friends[0]].userName), \(meetUpEntity.participants[meetUpEntity.friends[1]].userName )"
                + " and \(meetUpEntity.participants.count-2) others are meeting up."
        }
        return feedText
    }
}