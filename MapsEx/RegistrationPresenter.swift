//
//  RegistrationPresenter.swift
//  MapsEx
//
//  Created by Pradeep on 08/11/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
class RegistrationPresenter{
    var registrationManager: RegistrationManager?
    var sessionManager: SessionManager?
    init(){
        
    }
    
    func registerUser(userInfo: UserInfo,registrationCompleteHandler: (error: NSError?)->()){
        self.saveUserDetails(userInfo)
        registrationManager?.registerUser(userInfo, completionHandler: {[unowned self] (error) -> () in
                registrationCompleteHandler(error: error)
        })
    }
    
    func saveUserDetails(userInfo: UserInfo){
        sessionManager?.setUserInfo(userInfo)
    }
    
    func validateUserDetails(phoneNumber: String,userName: String,email: String)-> Bool{
        if (phoneNumber.isEmpty == false && userName.isEmpty == false && email.isEmpty == false) {
            return true
        }
        return false
    }
}