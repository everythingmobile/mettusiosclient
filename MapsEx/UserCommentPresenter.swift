//
//  UserCommentPresenter.swift
//  MapsEx
//
//  Created by Pradeep on 31/01/15.
//  Copyright (c) 2015 pradeep. All rights reserved.
//

import Foundation
class UserCommentPresenter {
    init(){
        
    }
    
    var userCommentManager : UserCommentManager?
    var sessionManager: SessionManager?
    
    func getMeetUpComments(#meetUpId: String,completionHandler : (([UserAndUserComment]) -> ())){
        if let  phoneInfo = sessionManager?.userSession?.userInfo?.phoneInfo{
            var userCommentHelper = UserCommentHelper(meetUpId: meetUpId, countryCode: phoneInfo.countryCode, phoneNumber: phoneInfo.phoneNumber)
            userCommentManager?.getMeetUpComments(userCommentHelper, completionHandler: completionHandler)
        }
    }
    
    func addUserComment(meetUpId: String,comment: String){
        var userComment = UserComment()
        userComment.meetUpId = meetUpId
        userComment.commentTime = NSDate()
        userComment.commentId = 0
        userComment.comment = comment
        userComment.phoneInfo = SessionManager.sharedsessionManager.userSession!.userInfo!.phoneInfo
        userCommentManager?.addUserComment(userComment)
    }
}
