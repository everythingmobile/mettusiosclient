//
//  MeetUpEntity.swift
//  SMS_VERF
//
//  Created by Pradeep on 20/09/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
class MeetUpEntity   {
    var  meetUpLocation: Location
    var  meetUpId : String
    var friends : [Int]
    var participants : [UserInfo]
    var startUpTime: NSDate
    var adminPhoneInfo: PhoneInfo
    
    init(){
        meetUpLocation = Location()
        meetUpId = ""
        friends = [Int]()
        participants = [UserInfo]()
        startUpTime = NSDate()
        adminPhoneInfo = PhoneInfo()
    }
    
    init(fromDictionary feed: NSDictionary){
        self.meetUpLocation = Location(fromDictionary: feed["meetUpLocation"] as! NSDictionary)
        self.meetUpId = feed["meetUpId"] as! String
        if let friendsList = feed["friends"] as? NSArray{
            self.friends = MeetUpEntity.createIndexList(fromArray: friendsList)
        }
        else{
            self.friends = [Int]()
        }
        if let participantList = feed["participants"] as? NSArray{
            self.participants = MeetUpEntity.createUserList(fromArray: participantList)
        }
        else{
            self.participants = [UserInfo]()
        }
        
        if let timeInMiliseconds  = feed["startUpTime"] as? NSTimeInterval {
            self.startUpTime = NSDate(timeIntervalSince1970: timeInMiliseconds/1000)
        }
        else{
            self.startUpTime = NSDate()
        }
        
        if let phoneInfoDict = feed["adminPhoneInfo"] as? NSDictionary {
            self.adminPhoneInfo = PhoneInfo(fromDictionary: phoneInfoDict)
        }
        else{
            self.adminPhoneInfo = PhoneInfo()
        }
    }
    
    class func createUserList(fromArray arr : NSArray) -> Array<UserInfo>{
        var userList = Array<UserInfo>()
        for item in arr{
            if let dict = item as? NSDictionary{
                var user = UserInfo(fromDictionary: dict)
                userList.append(user)
            }
        }
        return userList
    }
    
    class func createIndexList(fromArray arr : NSArray) -> Array<Int>{
        var list = Array<Int>()
        for item in arr{
            if let number = item as? Int{
                list.append(number)
            }
        }
        return list
    }
    
}