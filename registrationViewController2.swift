//
//  registrationViewController2.swift
//  MeetUp
//
//  Created by Pravin Gadakh on 9/20/14.
//  Copyright (c) 2014 Pravin Gadakh. All rights reserved.
//

import UIKit
import Foundation

class registrationViewController2: UIViewController,UITextViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate {
    var countryCodesHelper: CountryCodesHelper = CountryCodesHelper()
    var selectedValue:String = ""
    @IBOutlet weak var phoneNumberFT: UITextField!

    var countryPickerView: UIPickerView = UIPickerView()
    @IBOutlet weak var countryNameFT: UITextField!
    
    @IBOutlet weak var registerButton: UIButton!
    
    @IBAction func editingChanged (sender : AnyObject) {
        var phoneNumber = self.phoneNumberFT.text
        if phoneNumber.isEmpty == false {
            self.registerButton.enabled = true
        }
        else {
            self.registerButton.enabled = false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        countryCodesHelper.getCountryCodes()
        countryNameFT.inputView = countryPickerView
        countryPickerView.delegate = self
        countryPickerView.dataSource = self
        self.registerButton.enabled = false
        self.countryNameFT.text = countryCodesHelper.getDefaultCountry()

        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent){
        self.view.endEditing(true)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "push" {
            var verificationController : verificationViewController = segue.destinationViewController as! verificationViewController
            verificationController.phone =  self.phoneNumberFT.text
            verificationController.countryCode = countryCodesHelper.countryCodes[self.countryNameFT.text]!.dial_code
            verificationController.flag = 1
        }
    }
    
    @IBAction func touchDown(sender: AnyObject) {
        self.view.resignFirstResponder()
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countryCodesHelper.countries.count
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        var text = countryCodesHelper.countries[row]
        countryNameFT.text = text
        countryNameFT.resignFirstResponder()
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return countryCodesHelper.countries[row]
    }
    
}
