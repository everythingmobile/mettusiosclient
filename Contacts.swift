//
//  Contacts.swift
//  MeetUp
//
//  Created by Pravin Gadakh on 9/20/14.
//  Copyright (c) 2014 Pravin Gadakh. All rights reserved.
//
import Foundation

class Contacts {
    var phoneInfo: PhoneInfo = PhoneInfo()
    var name: String = ""
    
    init (name: String, phoneInfo: PhoneInfo) {
        self.name = name
        self.phoneInfo = phoneInfo
    }
}
