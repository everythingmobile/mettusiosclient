//
//  UserMeetUpsRequest.swift
//  SMS_VERF
//
//  Created by Pradeep on 20/09/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
class UserMeetUpsRequest{
    var  countryCode : String
    var phoneNumber: String
    var pageNum : String
    
    init(countryCode code: String,phoneNumber: String,pageNum : String){
        self.countryCode = code
        self.phoneNumber = phoneNumber
        self.pageNum = pageNum
    }
    
    func toDict()->Dictionary<String,AnyObject>{
        var dict =  Dictionary<String,AnyObject>()
        dict["pageNum"] = self.pageNum
        dict["phoneNumber"] = self.phoneNumber
        dict["countryCode"] =  self.countryCode
        return dict
    }
}