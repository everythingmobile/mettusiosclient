//
//  ContactsController.swift
//  MeetUp
//
//  Created by Pravin Gadakh on 9/20/14.
//  Copyright (c) 2014 Pravin Gadakh. All rights reserved.
//

import UIKit
import AddressBook
import Foundation

class ContactsController: UITableViewController,UITableViewDataSource,UITableViewDelegate,SWTableViewCellDelegate {
    var rowCount: Int = 5
    var friends = Array<UserInfo>()
    var contactList = Array<Contacts>()
    var contactsPresenter: ContactsPresenter?
    var countryCode: String = ""
    var phone : String = ""
    var flag : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        println("here")
        self.contactsPresenter = ContactsPresenter()
        self.title = "Contacts"
        /*self.view!.backgroundColor = UIColor(red: 29, green: 204, blue: 198, alpha: 1)
        self.navigationItem.titleView!.tintColor = UIColor(red: 29, green: 204, blue: 198, alpha: 1)*/
        self.contactsPresenter!.sessionManager = SessionManager.sharedsessionManager
        self.contactsPresenter!.verificationManager = VerificationManger()
        SessionManager.sharedsessionManager.loadUserInfoFromStore()
        self.contactsPresenter?.retrieveFriendContacts(completionHandler: {[unowned self] (friends) -> () in
            
            NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                self.friends = friends
                self.tableView.reloadData()
            })
        })
        self.tableView.allowsMultipleSelectionDuringEditing = false
        tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0)
        //verifyFriends()
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.friends.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:SWTableViewCell = self.tableView.dequeueReusableCellWithIdentifier("cell") as! SWTableViewCell
        cell.contentView.frame = cell.bounds
        cell.contentView.autoresizingMask = UIViewAutoresizing.FlexibleHeight | UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleLeftMargin | UIViewAutoresizing.FlexibleRightMargin | UIViewAutoresizing.FlexibleTopMargin | UIViewAutoresizing.FlexibleBottomMargin
        cell.textLabel!.text = self.friends[indexPath.row].userName
        cell.detailTextLabel!.text = self.friends[indexPath.row].phoneInfo.phoneNumber
        cell.leftUtilityButtons = leftButtons() as [AnyObject]
        cell.delegate = self
        return cell
    }
    

    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if(editingStyle == UITableViewCellEditingStyle.Delete){
            self.friends.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath.row], withRowAnimation: UITableViewRowAnimation.Automatic)
        }
    }
    
    func leftButtons() -> NSArray{
        var lb = NSMutableArray()
        lb.sw_addUtilityButtonWithColor(UIColor(red: 1.0, green: 0.231, blue: 0.188, alpha: 1), title: "Delete")
        return lb
    }
    
    func swipeableTableViewCell(cell: SWTableViewCell!, didTriggerLeftUtilityButtonWithIndex index: Int) {
        switch(index){
        case 0:
            let indexPath = self.tableView.indexPathForCell(cell)
            self.friends.removeAtIndex(indexPath!.row)
            tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: UITableViewRowAnimation.Automatic)
            println("Delet was pressed")
        default:
            println("Nothing")
        }
    }
    
    func swipeableTableViewCell(cell: SWTableViewCell!, canSwipeToState state: SWCellState) -> Bool {
        if(state == SWCellState.CellStateLeft){
            return true
        }
        return false
    }
    
}