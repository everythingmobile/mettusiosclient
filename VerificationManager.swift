//
//  VerificationManager.swift
//  SMS_VERF
//
//  Created by sudheer kumar meesala on 9/7/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//
import Foundation

class VerificationManger{
    init(){
        
    }
    let baseUrl = Constants.baseUrl
    let persistenceStore = PersistenceStore()
    internal func verifyFriends(userAndContacts : UserAndContacts) -> Request {
        //println(self.JSONStringify(userAndContacts))
        //println(userAndContacts.toDict())
        //var parameters = userAndContacts as Dictionary<String,AnyObject>
        return request(.POST, baseUrl+"verifyFriends", parameters: userAndContacts.toDict(), encoding: .JSON)
    }
    
    func JSONStringify(jsonObj: AnyObject) -> String {
        var e: NSError?
        let jsonData: NSData! = NSJSONSerialization.dataWithJSONObject(
            jsonObj,
            options: NSJSONWritingOptions(0),
            error: &e)
        if e != nil {
            return ""
        } else {
            return NSString(data: jsonData, encoding: NSUTF8StringEncoding)! as String
        }
    }
    
    internal func verifyUser(verification : Verification,completionHandler: (tokens: Tokens?,error: NSError?)->()) {
        request(.POST, baseUrl+"verifyUser", parameters: verification.toDict(), encoding: .JSON)
        .responseJSON { [unowned self](request, response, data, error) -> Void in
            if(error == nil){
                if let dict = data as? NSDictionary{
                    var tokens = Tokens(fromDictionary: dict)
                    self.persistenceStore.persistIsLoggedInFlag()
                    completionHandler(tokens: tokens,error:  nil)
                }
                else{
                    completionHandler(tokens: Tokens(), error: NSError())
                }
            }
            else{
                completionHandler(tokens: Tokens(), error: error)
            }
        }
    }
    
}