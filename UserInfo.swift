//
//  UserInfo.swift
//  SMS_VERF
//
//  Created by sudheer kumar meesala on 9/7/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//
import Foundation

class UserInfo: Equatable {
    
    var phoneInfo : PhoneInfo
    var userName : String
    var emailId : String
    var registered : Bool
    
    init (userName: String, phoneInfo : PhoneInfo,emailId : String,registered : Bool = false) {
        self.userName = userName
        self.phoneInfo = phoneInfo
        self.emailId = emailId
        self.registered = registered
    }
    
    required init() {
        self.userName = ""
        self.phoneInfo = PhoneInfo()
        self.emailId = ""
        self.registered = false
    }
    
    func toDict () -> Dictionary<String,AnyObject> {
        var dict =  Dictionary<String,AnyObject>()
        dict["phoneInfo"] = self.phoneInfo.toDict()
        dict["userName"] = self.userName
        dict["emailId"] = self.emailId
        dict["registered"] = self.registered
        return dict
    }
    
    init(fromDictionary dict : NSDictionary){
        self.phoneInfo = PhoneInfo(fromDictionary: dict["phoneInfo"] as! NSDictionary)
        self.userName =  dict["userName"] as! String
        self.emailId = dict["emailId"] as! String
        self.registered = dict["registered"] as! Bool
    }
}

func == (lhs: UserInfo, rhs: UserInfo) -> Bool {
    return lhs.phoneInfo == rhs.phoneInfo
}
